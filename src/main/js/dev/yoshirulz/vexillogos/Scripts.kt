package dev.yoshirulz.vexillogos

import org.w3c.dom.*
import kotlinx.browser.document
import kotlinx.browser.window
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

const val NO_ERR_MESG = "null (no message given)"
const val PACKAGE = "VexilLogos.dev.yoshirulz.vexillogos"
val x100: (String) -> Int = { it.toInt() * 100 }

var customFrames = mutableListOf<Int>()
lateinit var designData: Design
var nextCustomFrameIndex = 0
var isHoistReversed = false
var isVertical = false
var presetModified = false

fun addCustomFrameAndUpdate() = (nextCustomFrameIndex++).let { index ->
	customFrames.add(index)
	document.gEBI("custom-frames")!!.appendChild(document.createElement("fieldSet").also { fieldSet ->
		fieldSet.id = "custom-frame-$index"
		fieldSet.appendChild(document.createElement("legend").also { it.innerHTML = "Custom #$index" })
		fieldSet.appendChild((document.createElement("button") as HTMLButtonElement).also { it.innerHTML = "Remove frame"
			it.onclick = { removeCustomFrameAndUpdate(index) }
		})
		fieldSet.appendChild(document.createElement("br"))
		fieldSet.appendChild((document.createElement("label") as HTMLLabelElement).also { label ->
			label.innerHTML = "Fill: "
			label.appendChild((document.createElement("input") as HTMLInputElement).also {
				it.type = "color"; it.name = "fill" })
			label.onclick = { updateView() }
		})
		fieldSet.appendChild(document.createElement("br"))
		fieldSet.appendChild((document.createElement("label") as HTMLLabelElement).also { label ->
			label.innerHTML = "Path definition: "
			label.appendChild((document.createElement("input") as HTMLInputElement).also {
				it.type = "text"; it.name = "path"; it.placeholder = "M50,100 l50,-50 v100 Z" })
			label.onclick = { updateView() }
		})
	})
}

fun addSectionChild(sectionID: String, childClass: String) {
	val section = document.gEBI(sectionID) as HTMLFieldSetElement
	val typeElem = getPropChildOf(section, "$childClass-type")
	section.removeChild(typeElem)
	section.removeChild(filterElemsByClass(section.getChildList(), childClass).first())
	(document.gEBI("template-$childClass-${typeElem.getFirstAsSelect().value}")!!.firstElementChild as HTMLElement)
		.cloneElement().also {
			section.appendChild(it)
			it.setAttribute("id", "$sectionID-$childClass")
			filterElemsByTag(it.getChildList(), "button").first().run {
				dataset["removeSectionChildAndUpdate"] = childClass
				onclick = { removeSectionChildAndUpdate(sectionID, childClass) }
			}
			filterElemsByTag(it.getChildList(), "fieldset").firstOrNull()?.also { fieldSet ->
				removeSectionChild(it.id, fieldSet.getAttribute("class")!!)
			}
			if (randomiseOnSelect) randomiseRecursively(it as HTMLFieldSetElement)
		}
}

fun addSectionChildAndUpdate(sectionID: String, childClass: String) {
	addSectionChild(sectionID, childClass)
	updateView()
}

fun <T: SelectOption<T>> arrayMappedByValString(vararg arrays: Array<out T>): Map<String, T> {
	val map = mutableMapOf<String, T>()
	arrays.forEach { a -> a.forEach { map[it.valString] = it } }
	return map
}

fun blazon() {
	try {
		UIElements.blazonTextarea.innerHTML = designData.blazon().concat().replaceFirstChar(Char::uppercaseChar)
	} catch (err: NotImplementedError) {
		logNonfatalError("blazon", 1, err.message)
	}
}

fun exportSVG() {
	UIElements.exportImage.src = "data:image/svg+xml;utf8,${UIElements.viewParent.innerHTML.replace("#", "%23")}"
}

fun filterElemsByClass(elements: List<HTMLElement>, className: String) =
	elements.filter { it.classList.contains(className) }

fun filterElemsByTag(elements: List<HTMLElement>, tagName: String) =
	elements.filter { it.tagName.lowercase() == tagName }

@JsExport
fun findBugs(n: Int = 500, r: Int = 10) {
	(document.g1EBN(UIElements.randomiseFlagColoursCheckboxName)!! as HTMLInputElement).checked = false
	limitToFlagColours = false
	(document.g1EBN(UIElements.randomiseLimitColoursCheckboxName)!! as HTMLInputElement).checked = false
	limitMaxRandomColours = false
	randomColourCount = 0
	(document.g1EBN(UIElements.randomiseChargesCheckboxName)!! as HTMLInputElement).checked = true
	randomiseCharges = true
	(document.g1EBN(UIElements.randomiseExtrasCheckboxName)!! as HTMLInputElement).checked = true
	randomiseExtras = true
	(document.g1EBN(UIElements.reselectExtrasCheckboxName)!! as HTMLInputElement).checked = true
	randomlyReselectExtras = true
	(document.g1EBN(UIElements.randomiseDesignCheckboxName)!! as HTMLInputElement).checked = true
	randomiseDesign = true
	findBugsIntern(n, r)
}

fun findBugsIntern(n: Int, r: Int) {
	if (n == 0) return
	randomiseAll()
	window.setTimeout({ findBugsIntern(n - 1, r) }, r)
}

fun getFillFromChildrenOf(element: HTMLFieldSetElement) = Colour(getPropFromChildrenOf(element, "fill").uppercase())

fun getPropChildOf(element: HTMLFieldSetElement, property: String) =
	filterElemsByTag(element.getChildList(), "label").first { it.firstElementChild!!.asDynamic().name == property }

fun getPropFromChildrenOf(element: HTMLFieldSetElement, property: String) =
	getPropChildOf(element, property).firstElementChild!!.let { child ->
		(child as? HTMLInputElement)?.value ?: (child as HTMLSelectElement).value
	}

fun hideSection(sectionID: String, triggerUpdate: Boolean = true) {
	document.gEBI(sectionID)!!.hidden = true
	if (triggerUpdate) updateView()
}

fun loadPreset() {
	if (!presetModified || window.confirm("Overwrite everything with preset?")) {
		UIElements.presetSelect.value.writeToUI()
		updateView()
		presetModified = false
	}
}

fun logInfo(message: String) = console.log(message)

fun logNonfatalError(function: String, n: Int, message: String? = null) =
	console.log("[$function.$n] ${message ?: NO_ERR_MESG}")

/**
 * @param t angle in half-revolutions (allows easier computation in genSimpleStarPoints)
 * @see SimpleCharge.genSimpleStarPoints
 */
fun polarToCart(r: Int, t: Double) = DoublePoint(r * cos(PI * t), r * sin(PI * t))

fun randomiseAll() {
	if (randomiseDesign) {
		UIElements.aspectSelect.value = AspectsSpinner.getRandom()
		UIElements.reverseHoistCheckbox.checked = randChanceOneIn(5)
		UIElements.designSelect.value = DesignsSpinner.getRandom()
	}
	RandomColourSpinner.refill()
	randomiseRecursively(document.gEBI(UIElements.designSelect.value.valString) as HTMLFieldSetElement)
	if (randomlyReselectExtras) {
		PatternOptElemsSpinner.patternOptElems.forEach { it.hidden = true }
		var count = ExtrasCountSpinner.getRandom()
		while (count > 0) PatternOptElemsSpinner.getRandom().also {
			if (it.hidden && it.id != ExtraPatternOption.CustomFrame.valString) {
				showPattern(it)
				count--
			}
		}
	} else if (randomiseExtras) {
		PatternOptElemsSpinner.patternOptElems.filterNot { it.hidden }.forEach { randomiseRecursively(it) }
	}
	updateDesign()
	randomiseRandomButton()
}

fun randomiseRandomButton() {
	UIElements.randomiseButton.innerHTML = "${DieFaceSpinner.getRandom()} Random-itate! ${DieFaceSpinner.getRandom()}"
}

fun removeCustomFrameAndUpdate(index: Int) {
	document.gEBI("custom-frame-$index")!!.remove()
	customFrames.remove(index)
	updateView()
}

fun removeSectionChild(sectionID: String, childClass: String) {
	val section = document.gEBI(sectionID)!!
	section.removeChild(filterElemsByClass(section.getChildList(), childClass).first())
	document.gEBI("template-$childClass-add")!!.getChildList().forEach { section.appendChild(it.cloneElement()) }
	(section.children[section.childElementCount - 2] as HTMLButtonElement).run {
		dataset["addSectionChildAndUpdate"] = childClass
		onclick = { addSectionChildAndUpdate(sectionID, childClass) }
	}
}

fun removeSectionChildAndUpdate(sectionID: String, childClass: String) {
	removeSectionChild(sectionID, childClass)
	updateView()
}

fun resetFieldsets(section: HTMLFieldSetElement): Unit = section.getChildList().forEach {
	when (it.tagName.lowercase()) {
		"fieldset" -> if (it.childElementCount > 0) resetFieldsets(it as HTMLFieldSetElement)
		else removeSectionChild(section.id, it.className)
		"button" -> if (it.classList.contains("removeSectionChildAndUpdate")) {
			(it as HTMLButtonElement).click()
		}
	}
}

fun showPattern(section: HTMLFieldSetElement) {
	section.hidden = false
	if (randomiseOnSelect) {
		resetFieldsets(section)
		randomiseRecursively(section)
	}
	updateView()
}

fun showPatternFromInput() =
	showPattern(document.gEBI(UIElements.patternSelect.value.valString) as HTMLFieldSetElement)

fun updateDesign(fromButton: Boolean = false) {
	DesignOption.MiscDesigns.Field.getLookupMap().values.forEach { document.gEBI(it.valString)!!.hidden = true }
	val section = document.gEBI(UIElements.designSelect.value.valString) as HTMLFieldSetElement
	section.hidden = false
	if (fromButton && randomiseOnSelect) {
		resetFieldsets(section)
		randomiseRecursively(section)
	}
	updateView()
}

fun updatePaneWidth() = UIElements.optsWrapper.style.setProperty("width", "${UIElements.paneWidthNumBox.value}rem")

fun updateRandOpts(opt: String) = (document.g1EBN(opt) as HTMLInputElement).also {
	when (opt) {
		UIElements.randomiseChargesCheckboxName -> randomiseCharges = it.checked
		UIElements.randomiseDesignCheckboxName -> randomiseDesign = it.checked
		UIElements.randomiseExtrasCheckboxName -> randomiseExtras = it.checked
		UIElements.randomiseFlagColoursCheckboxName -> limitToFlagColours = it.checked
		UIElements.randomiseLimitColoursCheckboxName -> limitMaxRandomColours = it.checked
		UIElements.randomiseNumColoursNumBoxName -> randomColourCount = it.value.toInt()
		UIElements.randomiseOnSelectCheckboxName -> randomiseOnSelect = it.checked
		UIElements.reselectExtrasCheckboxName -> randomlyReselectExtras = it.checked
		else -> throw IllegalArgumentException(opt)
	}
}

fun updateView() {
	logInfo("---==:| REDRAW |:==---")
	presetModified = true
	val size = UIElements.aspectSelect.value.valString.split("-").reversed().map(x100).let { WidthHeight(it[0], it[1]) }
	val view = createSVGBBox("flag").also {
		it.setAttributeNS(null, "viewBox", "0 0 ${size.w} ${size.h}")
		it.setAttributeNS(XML_NS, "xmlns", SVG_NS)
	}
	UIElements.viewParent.removeChild(UIElements.viewParent.firstChild!!)
	UIElements.viewParent.appendChild(view)
	isHoistReversed = UIElements.reverseHoistCheckbox.checked
	view.style.setProperty("transform",
		"scaleX(${if (isHoistReversed) -1 else 1}) rotate(${if (isVertical) 90 else 0}deg)")
	val currentDesign = UIElements.designSelect.value
	designData = Design.parseFromUI(currentDesign)!!
	try {
		view.appendChild(designData.genSVG(size).also {
			SVGBBoxAttrs("0 0 ${size.w} ${size.h}", size.w, size.h).applyTo(it)
		})
	} catch (err: NotImplementedError) {
		logNonfatalError("updateView", 1, err.message)
	}
	try {
		PatternOptElemsSpinner.patternOptElems.filterNot { it.hidden }.forEach {
			view.appendChild(ExtraPattern.parseFromUI(
				ExtraPatternOption.Canton.getLookupMap().getValue(it.id),
				currentDesign).genSVG(size))
		}
	} catch (err: NotImplementedError) {
		logNonfatalError("updateView", 2, err.message)
	}
}
