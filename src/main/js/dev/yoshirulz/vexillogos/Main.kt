package dev.yoshirulz.vexillogos

import dev.yoshirulz.vexillogos.Bordure.Companion.BordureWidth
import dev.yoshirulz.vexillogos.Canton.Companion.CantonContentType
import dev.yoshirulz.vexillogos.Canton.Companion.CantonWidth
import dev.yoshirulz.vexillogos.Chevron.Companion.ChevronWidth
import dev.yoshirulz.vexillogos.Cross.Companion.CrossShape
import dev.yoshirulz.vexillogos.DesignOption.*
import dev.yoshirulz.vexillogos.Frame.Companion.FrameShape
import dev.yoshirulz.vexillogos.HNBand.Companion.HDuobandRatio
import dev.yoshirulz.vexillogos.HNBand.Companion.HTribandRatio
import dev.yoshirulz.vexillogos.HoistBand.Companion.HoistBandWidth
import dev.yoshirulz.vexillogos.Orle.Companion.OrleWidth
import dev.yoshirulz.vexillogos.VNBand.Companion.VDuobandRatio
import dev.yoshirulz.vexillogos.VNBand.Companion.VTribandRatio
import kotlinx.html.*
import kotlinx.html.dom.append
import org.w3c.dom.*
import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.dom.clear
import kotlinx.html.js.onClickFunction
import kotlinx.html.js.onMouseOutFunction
import kotlinx.html.js.onMouseOverFunction

object UIElements {
	const val aspectSelectName = "aspect"
	const val blazonButtonID = "blazon-button"
	const val blazonTextareaID = "blazon-output"
	const val bordureWidthSelectName = "bordure-width"
	const val cantonWidthSelectName = "canton-width"
	const val chevronWidthSelectName = "chevron-width"
	const val crossShapeSelectName = "cross-shape"
	const val designOptsID = "opts"
	const val designSelectName = "design"
	const val exportImageID = "export-img"
	const val frameShapeSelectName = "frame-shape"
	const val hoistBandWidthSelectName = "hoist-band-width"
	const val orleWidthSelectName = "orle-width"
	const val patternSelectName = "pattern"
	const val paneWidthNumBoxName = "toolpane-width"
	const val presetSelectName = "flag-preset"
	const val randomiseButtonID = "randomise-button"
	const val randomiseChargesCheckboxName = "randomise-charges"
	const val randomiseDesignCheckboxName = "randomise-design"
	const val randomiseExtrasCheckboxName = "randomise-extras"
	const val randomiseFlagColoursCheckboxName = "randomise-flag-colours"
	const val randomiseLimitColoursCheckboxName = "randomise-limit-colours"
	const val randomiseNumColoursNumBoxName = "randomise-num-colours"
	const val randomiseOnSelectCheckboxName = "randomise-on-select"
	const val randomiseOptClass = "randomise-opt"
	const val randomiseOptsID = "randomise"
	const val reselectExtrasCheckboxName = "randomise-reselect-extras"
	const val reverseHoistCheckboxName = "reverse-hoist"

	val optsWrapper = document.gEBI("opts-wrapper")!!
	val templateContainer = document.gEBI("templates")!!
	val viewParent = document.gEBI("view")!!

	lateinit var aspectSelect: EnumLimitedSelect<AspectRatio>
	lateinit var barruletsOpts: HTMLFieldSetElement
	lateinit var blazonTextarea: HTMLTextAreaElement
	lateinit var bordureWidthSelect: EnumLimitedSelect<BordureWidth>
	lateinit var cantonOpts: HTMLFieldSetElement
	lateinit var cantonWidthSelect: EnumLimitedSelect<CantonWidth>
	lateinit var chevronWidthSelect: EnumLimitedSelect<ChevronWidth>
	lateinit var crossOpts: HTMLFieldSetElement
	lateinit var crossShapeSelect: EnumLimitedSelect<CrossShape>
	lateinit var designSelect: EnumLimitedSelect<DesignOption>
	lateinit var exportImage: HTMLImageElement
	lateinit var frameShapeSelect: EnumLimitedSelect<FrameShape>
	lateinit var hoistBandWidthSelect: EnumLimitedSelect<HoistBandWidth>
	lateinit var orleWidthSelect: EnumLimitedSelect<OrleWidth>
	lateinit var palletsOpts: HTMLFieldSetElement
	lateinit var patternSelect: EnumLimitedSelect<ExtraPatternOption>
	lateinit var paneWidthNumBox: HTMLInputElement
	lateinit var presetSelect: EnumLimitedSelect<FlagPreset>
	lateinit var randomiseButton: HTMLButtonElement
	lateinit var randomiseOnSelectCheckbox: HTMLInputElement
	lateinit var reverseHoistCheckbox: HTMLInputElement
}

fun main() {
	window.onload = {
		js("window.findBugs = $PACKAGE.findBugs")
		populateDOM()
		pointToUI()
		postPopulate()
		loadPreset()
	}
}

fun populateDOM() {
	UIElements.viewParent.clear()
	UIElements.viewParent.append { svg {} }
	UIElements.optsWrapper.append {
		fieldSet { legend { +"Design options" }; id = UIElements.designOptsID
			vxSelectLabelGen(this, UIElements.presetSelectName, "Load preset design:",
				PresetDesignSpinner.getRandom(), FlagPreset.values()
			); button { +"Load"
				onClickFunction = { loadPreset() }
				type = ButtonType.button
			}; br()
			vxSelectLabelGen(this, UIElements.aspectSelectName, "Aspect ratio (width–length): ",
				AspectRatio.OneTwo, AspectRatio.values()); br()
			label { +"Hoist on right (reflect in x): "
				checkBoxInput { name = UIElements.reverseHoistCheckboxName }
			}; br()
			vxSelectLabelGen<DesignOption>(this, UIElements.designSelectName, "Primary design: ",
				MiscDesigns.Cross, groupedValues = mapOf(
					"Misc." to MiscDesigns.values(),
					"Horizontal bands" to HBands.values(),
					"Vertical bands" to VBands.values())); br()
			vxSelectLabelGen(this, UIElements.patternSelectName, "Add pattern: ",
				ExtraPatternOption.Canton, ExtraPatternOption.values()
			); button { +"Add"
				onClickFunction = { showPatternFromInput() }
				type = ButtonType.button
			}
		}; br()

		fieldSet { legend { +"Primary design: field" }; id = MiscDesigns.Field.valString
			hidden = true
			vxFillLabel(this); br()
			vxChargeFieldset(this)
		}

		fieldSet { legend { +"Primary design: quadrisection" }; id = MiscDesigns.Quadrisection.valString
			hidden = true
			fieldSet { legend { +"Canton quarter" }; id = "quad-0-0"
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
			fieldSet { legend { +"Lower-hoist quarter" }; id = "quad-0-1"
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
			fieldSet { legend { +"Upper-fly quarter" }; id = "quad-1-0"
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
			fieldSet { legend { +"Lower-fly quarter" }; id = "quad-1-1"
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
		}

		fieldSet { legend { +"Primary design: party per saltire" }; id = MiscDesigns.PerSaltire.valString
			hidden = true
			fieldSet { legend { +"Canton quarter" }; id = "per-saltire-2"
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
			fieldSet { legend { +"Chief quarter" }; id = "per-saltire-1"
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
			fieldSet { legend { +"Fly quarter" }; id = "per-saltire-3"
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
			fieldSet { legend { +"Base quarter" }; id = "per-saltire-0"
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
		}

		fieldSet { legend { +"Primary design: gyronny (of eight)" }; id = MiscDesigns.Gyronny.valString
			hidden = true
			vxFillLabel(this, "Field fill: "); br()
			label { +"Dexter chief fill: "
				colorInput { name = "chief-fill" }
			}
		}

		fieldSet { legend { +"Primary design: party per bend" }; id = MiscDesigns.PerBend.valString
			hidden = true
		}

		fieldSet { legend { +"Primary design: bend" }; id = MiscDesigns.Bend.valString
			hidden = true
		}

		fieldSet { legend { +"Primary design: pall" }; id = MiscDesigns.Pall.valString
			hidden = true
		}

		fieldSet { legend { +"Primary design: saltire" }; id = MiscDesigns.Saltire.valString
			hidden = true
			vxFillLabel(this, "Field fill: "); br()
			label { +"Cross fill: "
				colorInput { name = "saltire-fill" }
			}
		}

		fieldSet { legend { +"Primary design: cross" }; id = MiscDesigns.Cross.valString
			hidden = true
			vxSelectLabelGen(this, UIElements.crossShapeSelectName, "Shape: ",
				CrossShape.Nordic, CrossShape.values()); br()
			fieldSet { legend { +"Canton section" }; id = "cross-0-0"
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
			fieldSet { legend { +"Lower-hoist section" }; id = "cross-0-1"
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
			fieldSet { legend { +"Upper-fly section" }; id = "cross-1-0"
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
			fieldSet { legend { +"Lower-fly section" }; id = "cross-1-1"
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
			fieldSet { legend { +"Cross" }; id = "cross-stripes"
				vxFillLabel(this); br()
				fieldSet("single-fimb")
			}
			vxChargeFieldset(this)
		}

		fieldSet { legend { +"Primary design: bars/barrulets" }; id = HBands.Barrulets.valString
			hidden = true
			vxFillLabel(this, "Field fill: "); br()
			label { +"Chief fill: "
				colorInput { name = "barrulet-fill" }
			}; br()
			label { +"Count: "
				numberInput { name = "count"
					min = 1.toString()
					max = 15.toString()
					step = 1.toString()
					value = 13.toString()
				}
			}
		}

		fieldSet { legend { +"Primary design: horizontal duoband" }; id = HBands.HDuoband.valString
			hidden = true
			vxSelectLabelGen<HDuobandRatio>(this, "h2band-ratio", "Band ratio: ",
				HDuobandRatio.Fifths.TwoThree, arrayOf(HDuobandRatio.Halves.Halved),
				mapOf("Thirds" to HDuobandRatio.Thirds.values(), "Quarters" to HDuobandRatio.Quarters.values(),
					"Fifths" to HDuobandRatio.Fifths.values()))
		}

		fieldSet { legend { +"Primary design: fess / horizontal triband" }; id = HBands.HTriband.valString
			hidden = true
			vxSelectLabelGen<HTribandRatio>(this, "h3band-ratio", "Band ratio: ",
				HTribandRatio.Thirds.Trisected, arrayOf(HTribandRatio.Thirds.Trisected),
				mapOf("Quarters" to HTribandRatio.Quarters.values(), "Fifths" to HTribandRatio.Fifths.values()))
		}

		fieldSet { legend { +"Primary design: horizontal tetraband" }; id = HBands.HTetraband.valString
			hidden = true
			textInput { name = "h4band-ratio"
				disabled = true
				hidden = true
				value = "1-1-1-1"
			}
		}

		fieldSet { legend { +"Primary design: horizontal pentaband" }; id = HBands.HPentaband.valString
			hidden = true
			textInput { name = "h5band-ratio"
				disabled = true
				hidden = true
				value = "1-1-1-1-1"
			}
		}

		fieldSet { legend { +"Primary design: horizontal hexaband" }; id = HBands.HHexaband.valString
			hidden = true
			textInput { name = "h6band-ratio"
				disabled = true
				hidden = true
				value = "1-1-1-1-1-1"
			}
		}

		fieldSet { legend { +"Primary design: horizontal heptaband" }; id = HBands.HHeptaband.valString
			hidden = true
			textInput { name = "h7band-ratio"
				disabled = true
				hidden = true
				value = "1-1-1-1-1-1-1"
			}
		}

		fieldSet { legend { +"Primary design: pallets" }; id = VBands.Pallets.valString
			hidden = true
			vxFillLabel(this, "Field fill: "); br()
			label { +"Fly fill: "
				colorInput { name = "pallet-fill" }
			}; br()
			label { +"Count: "
				numberInput { name = "count"
					min = 1.toString()
					max = 20.toString()
					step = 1.toString()
					value = 17.toString()
				}
			}
		}

		fieldSet { legend { +"Primary design: vertical duoband" }; id = VBands.VDuoband.valString
			hidden = true
			vxSelectLabelGen<VDuobandRatio>(this, "v2band-ratio", "Band ratio: ",
				VDuobandRatio.Halves.Halved, arrayOf(VDuobandRatio.Halves.Halved),
				mapOf("Thirds" to VDuobandRatio.Thirds.values(), "Quarters" to VDuobandRatio.Quarters.values()))
		}

		fieldSet { legend { +"Primary design: pale / vertical triband" }; id = VBands.VTriband.valString
			hidden = true
			vxSelectLabelGen<VTribandRatio>(this, "v3band-ratio", "Band ratio: ",
				VTribandRatio.Thirds.Trisected, arrayOf(VTribandRatio.Thirds.Trisected),
				mapOf("Quarters" to VTribandRatio.Quarters.values(), "Fifths" to VTribandRatio.Fifths.values()))
		}

		fieldSet { legend { +"Primary design: vertical tetraband" }; id = VBands.VTetraband.valString
			hidden = true
			textInput { name = "v4band-ratio"
				disabled = true
				hidden = true
				value = "1-1-1-1"
			}
		}

		br(); fieldSet { legend { +"Secondary designs" }
			fieldSet { legend { +"Canton" }; id = ExtraPatternOption.Canton.valString
				hidden = true
				button { +"Remove"
					onClickFunction = { hideSection(ExtraPatternOption.Canton.valString) }
					type = ButtonType.button
				}; br()
				vxSelectLabelGen(this, UIElements.cantonWidthSelectName, "Width: ",
					CantonWidth.Auto, CantonWidth.values()); br()
				fieldSet("canton-content")
			}

			fieldSet { legend { +"Hoist band" }; id = ExtraPatternOption.HoistBand.valString
				hidden = true
				button { +"Remove"
					onClickFunction = { hideSection(ExtraPatternOption.HoistBand.valString) }
					type = ButtonType.button
				}; br()
				vxSelectLabelGen(this, UIElements.hoistBandWidthSelectName, "Width: ",
					HoistBandWidth.Third, HoistBandWidth.values()); br()
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}

			fieldSet { legend { +"Chevron" }; id = ExtraPatternOption.Chevron.valString
				hidden = true
				button { +"Remove"
					onClickFunction = { hideSection(ExtraPatternOption.Chevron.valString) }
					type = ButtonType.button
				}; br()
				vxSelectLabelGen(this, UIElements.chevronWidthSelectName, "Width: ",
					ChevronWidth.Third, ChevronWidth.values()); br()
				vxFillLabel(this); br()
				fieldSet("single-fimb")
			}

			fieldSet { legend { +"Frame" }; id = ExtraPatternOption.Frame.valString
				hidden = true
				button { +"Remove"
					onClickFunction = { hideSection(ExtraPatternOption.Frame.valString) }
					type = ButtonType.button
				}; br()
				vxSelectLabelGen(this, UIElements.frameShapeSelectName, "Shape: ", FrameShape.InvIsosceles, FrameShape.values()); br()
				vxFillLabel(this)
			}

			fieldSet { legend { +"Bordure" }; id = ExtraPatternOption.Bordure.valString
				hidden = true
				button { +"Remove"
					onClickFunction = { hideSection(ExtraPatternOption.Bordure.valString) }
					type = ButtonType.button
				}; br()
				vxSelectLabelGen(this, UIElements.bordureWidthSelectName, "Width: ",
					BordureWidth.Thin, BordureWidth.values()); br()
				vxFillLabel(this)
			}

			fieldSet { legend { +"Orle" }; id = ExtraPatternOption.Orle.valString
				hidden = true
				button { +"Remove"
					onClickFunction = { hideSection(ExtraPatternOption.Orle.valString) }
					type = ButtonType.button
				}; br()
				vxSelectLabelGen(this, UIElements.orleWidthSelectName, "Width: ",
					OrleWidth.Thin, OrleWidth.values()); br()
				vxFillLabel(this)
			}

			fieldSet { legend { +"Custom frame(s)" }; id = ExtraPatternOption.CustomFrame.valString
				hidden = true
				button { +"Remove"
					onClickFunction = { hideSection(ExtraPatternOption.CustomFrame.valString) }
					type = ButtonType.button
				}; button { +"Add frame"
					onClickFunction = { addCustomFrameAndUpdate() }
					type = ButtonType.button
				}; br()
				span { +"The viewbox is the aspect ratio scaled by 100 in both directions." }; br()
			}
		}; br()

		fieldSet { legend { +"Tools and misc." }
			fieldSet { legend { +"VexilLogos settings" }
				label { +"Randomise sections as they're added: "
					checkBoxInput(classes = UIElements.randomiseOptClass) {
						checked = true
						name = UIElements.randomiseOnSelectCheckboxName
					}
				}; br()
				label { +"Width of this pane: "
					numberInput { name = UIElements.paneWidthNumBoxName
						min = 10.toString()
						max = 50.toString()
						step = 5.toString()
						value = 25.toString()
					}
				}
			}
			fieldSet { legend { +"Randomise" }; id = UIElements.randomiseOptsID
				button { id = UIElements.randomiseButtonID
					onClickFunction = { randomiseAll() }
					onMouseOverFunction = { randomiseRandomButton() }
					onMouseOutFunction = { randomiseRandomButton() }
					type = ButtonType.button
				}; br()
				label { +"Only use \"flag colours\": "
					checkBoxInput(classes = UIElements.randomiseOptClass) {
						name = UIElements.randomiseFlagColoursCheckboxName
						checked = true
					}
				}; br()
				label { +"Limit to "
					numberInput(classes = UIElements.randomiseOptClass) {
						name = UIElements.randomiseNumColoursNumBoxName
						min = 2.toString()
						max = 5.toString()
						step = 1.toString()
						value = 3.toString()
					}
				}; +" "; label { +"colours: "
					checkBoxInput(classes = UIElements.randomiseOptClass) {
						name = UIElements.randomiseLimitColoursCheckboxName
					}
				}; br()
				label { +"Change charges: "
					checkBoxInput(classes = UIElements.randomiseOptClass) {
						name = UIElements.randomiseChargesCheckboxName
						checked = true
					}
				}; br()
				label { +"Also change chosen secondary designs: "
					checkBoxInput(classes = UIElements.randomiseOptClass) {
						name = UIElements.randomiseExtrasCheckboxName
						checked = true
					}
				}; br()
				label { +"Also reselect secondary designs: "
					checkBoxInput(classes = UIElements.randomiseOptClass) {
						name = UIElements.reselectExtrasCheckboxName
					}
				}; br()
				label { +"Also change primary design: "
					checkBoxInput(classes = UIElements.randomiseOptClass) {
						name = UIElements.randomiseDesignCheckboxName
					}
				}
			}
			fieldSet { legend { +"SVG Export" }
				span {
					button { +"Export current design"
						onClickFunction = { exportSVG() }
						type = ButtonType.button
					}; br()
					+"Right-click the image that appears and choose `Save As...` or similar."
				}; br()
				img {
					id = UIElements.exportImageID
					style = "left: 25%; position: relative; width: 50%;"
				}
			}
			fieldSet { legend { +"Blazon" }
				span {
					button { +"En-blazon-ise!"; id = UIElements.blazonButtonID
						onClickFunction = { blazon() }
						type = ButtonType.button
					}; +" (warning: imperfect)"
				}; br()
				label { +"Attempt to assign names to colours: "
					checkBoxInput { name = "blazon-name-colours"
						checked = true
					}
				}; br()
				label { +"Name colours using heraldric terms: "
					checkBoxInput { name = "blazon-name-tinctures" }
				}; br()
				textArea { id = UIElements.blazonTextareaID
					readonly = true
				}
			}
			fieldSet { legend { +"Info" }
				+"VexilLogos is provided for free, for everyone, forever, under the GNU "
				abbr { +"GPL"; title = "General Public Licence" }
				+" version 2 (or later): "
				a { +"full text"; href = "https://gitlab.com/YoshiRulz/VexilLogos/blob/master/LICENSE.md" }
				+", "
				a { +"FAQ"; href = "https://www.gnu.org/licenses/gpl-faq.html" }
				+"."
				br()

				+"In short, you can do whatever, as long as you attribute the "
				a { +"authors"; href = "https://gitlab.com/YoshiRulz/VexilLogos/graphs/master" }
				+" and include the "
				a { +"source code"; href = "https://gitlab.com/YoshiRulz/VexilLogos" }
				+" with any copies you share."
				br()

				+"If you click through to the "
				a { +"project page on GitLab"; href = "https://gitlab.com/YoshiRulz/VexilLogos" }
				+", you'll find useful things like the issue tracker."
				hr()

				+"The 1.0 goal for this project is to be able to make the flags of every UN "
				abbr {
					+"member"
					title = "(at the time of writing) Andorra, the UAE, Afghanistan, Antigua and Barbuda, Albania, " +
						"Armenia, Angola, Argentina, Austria, Australia, Azerbaijan, Bosnia and Herzegovina, " +
						"Barbados, Bangladesh, Belgium, Burkina Faso, Bulgaria, Bahrain, Burundi, Benin, Brunei, " +
						"Bolivia, Brazil, the Bahamas, Bhutan, Botswana, Belarus, Belize, Canada, the DRC, the CAR, " +
						"the Congo, Switzerland, Côte d'Ivoire, Chile, Cameroon, China, Colombia, Costa Rica, Cuba, " +
						"Cabo Verde, Cyprus, Czechia, Germany, Djibouti, Denmark, Dominica, the DR, Algeria, " +
						"Ecuador, Estonia, Egypt, Eritrea, Spain, Ethiopia, Finland, Fiji, FSM, France, Gabon, " +
						"the UK, Grenada, Georgia, Ghana, the Gambia, Guinea, Equatorial Guinea, Greece, Guatemala, " +
						"Guinea-Bissau, Guyana, Honduras, Croatia, Haiti, Hungary, Indonesia, Ireland, Israel, " +
						"India, Iraq, Iran, Iceland, Italy, Jamaica, Jordan, Japan, Kenya, Kyrgyzstan, Cambodia, " +
						"Kiribati, the Comoros, Saint Kitts and Nevis, the DPRK, the ROK, Kuwait, Kazakhstan, Laos, " +
						"Lebanon, Saint Lucia, Liechtenstein, Sri Lanka, Liberia, Lesotho, Lithuania, Luxembourg, " +
						"Latvia, Libya, Morocco, Monaco, Moldova, Montenegro, Madagascar, the Marshall Islands, " +
						"North Macedonia, Mali, Myanmar, Mongolia, Mauritania, Malta, Mauritius, the Maldives, " +
						"Malawi, Mexico, Malaysia, Mozambique, Namibia, Niger, Nigeria, Nicaragua, the Netherlands, " +
						"Norway, Nepal, Nauru, New Zealand, Oman, Panama, Peru, Papua New Guinea, the Philippines, " +
						"Pakistan, Poland, Portugal, Palau, Paraguay, Qatar, Romania, Serbia, Russia, Rwanda, " +
						"Saudi Arabia, the Solomon Islands, Seychelles, Sudan, Sweden, Singapore, Slovenia, " +
						"Slovakia, Sierra Leone, San Marino, Senegal, Somalia, Suriname, South Sudan, " +
						"São Tomé and Príncipe, El Salvador, Syria, eSwatini, Chad, Togo, Thailand, Tajikistan, " +
						"Timor-Leste, Turkmenistan, Tunisia, Tonga, Turkey, Trinidad and Tobago, Tuvalu, Tanzania, " +
						"Ukraine, Uganda, the USA, Uruguay, Uzbekistan, Saint Vincent and the Grenadines, Venezuela, " +
						"Vietnam, Vanuatu, Samoa, Yemen, South Africa, Zambia, and Zimbabwe"
				}
				+" and "
				abbr {
					+"observer"
					title = "(at the time of writing) Palestine and the Holy See (Vatican City)"
				}
				+" state."
			}
		}
	}
	UIElements.templateContainer.append {
		div { id = "template-band-charged"
			fieldSet { legend()
				vxFillLabel(this); br()
				vxChargeFieldset(this)
			}
		}
		div { id = "template-band-uncharged"
			fieldSet { legend()
				vxFillLabel(this)
			}
		}

		div { id = "template-canton-content-add"
			button(classes = "canton-content") { +"Set canton type to:"
				type = ButtonType.button
			}; vxSelectLabelGen(this, "canton-content-type", "", CantonContentType.Charge, CantonContentType.values())
		}
		div { id = "template-canton-content-charge"
			fieldSet("canton-content") { legend { +"Canton contents" }
				vxCantonContentTypeLabelGen(this, "canton-content-type", CantonContentType.Charge); br()
				vxRemoveButton(this); br()
				vxChargeFieldset(this)
			}
		}
		div { id = "template-canton-content-flag"
			fieldSet("canton-content") { legend { +"Canton contents" }
				vxCantonContentTypeLabelGen(this, "canton-content-type", CantonContentType.Flag); br()
				vxRemoveButton(this); br()
				vxSelectLabelGen(this, "flag", "Flag: ", FlagPreset.BritishPreset, FlagPreset.values())
			}
		}

		div { id = "template-single-fimb-add"
			button(classes = "single-fimb") { +"Add:"
				type = ButtonType.button
			}; vxSelectLabelGen(this, "single-fimb-type", "",
				SingleFimbriationWidth.Thin, SingleFimbriationWidth.values())
		}
		div { id = "template-single-fimb-thick"
			fieldSet("single-fimb") { legend { +"Fimbriation" }
				vxSingleFimbWidthLabelGen(this, "single-fimb-type", SingleFimbriationWidth.Thick); br()
				vxRemoveButton(this); br()
				vxFillLabel(this); br()
				label { +"Canton fill: "
					colorInput { name = "canton-fill" }
				}
			}
		}
		div { id = "template-single-fimb-thin"
			fieldSet("single-fimb") { legend { +"Fimbriation" }
				vxSingleFimbWidthLabelGen(this, "single-fimb-type", SingleFimbriationWidth.Thin); br()
				vxRemoveButton(this); br()
				vxFillLabel(this); br()
				label { +"Canton fill: "
					colorInput { name = "canton-fill" }
				}
			}
		}

		div { id = "template-charge-add"
			button(classes = "charge") { +"Add:"
				type = ButtonType.button
			}; vxSelectLabelGen(this, "charge-type", "", ChargeType.Simple, ChargeType.values())
		}
		div { id = "template-charge-circle"
			fieldSet("charge") { legend { +"Charge" }
				vxRemoveButton(this); br()
				vxChargeTypeLabelGen(this, "charge-type", ChargeType.Circle); br()
				label { +"Count: "
					numberInput { name = "count"
						min = 2.toString()
						max = 15.toString()
						step = 1.toString()
						value = 5.toString()
					}
				}; br()
				vxFillLabel(this); br()
				vxChargeShapeSelectLabel(this); br()
				label { +"Keep upright: "
					checkBoxInput { name = "keep-upright" }
				}; br()
				vxAngleLabel(this, "angle", "Subtended angle: ", 360); br()
				vxAngleLabel(this, labelText = "Rotation (of arc): "); br()
				fieldSet("encircled-charge")
			}
		}
		div { id = "template-charge-simple"
			fieldSet("charge") { legend { +"Charge" }
				vxRemoveButton(this); br()
				vxChargeTypeLabelGen(this, "charge-type", ChargeType.Simple); br()
				vxFillLabel(this); br()
				vxChargeShapeSelectLabel(this); br()
				vxAngleLabel(this)
			}
		}
		div { id = "template-charge-symbol"
			fieldSet("charge") { legend { +"Charge" }
				vxRemoveButton(this); br()
				vxChargeTypeLabelGen(this, "charge-type", ChargeType.Symbol); br()
				vxFillLabel(this); br()
				fieldSet("stacked-charge")
			}
		}

		div { id = "template-encircled-charge-add"
			button(classes = "encircled-charge") { +"Add:"
				type = ButtonType.button
			}; vxSelectLabelGen(this, "encircled-charge-type", "",
				ChargeType.Simple, arrayOf(ChargeType.Simple, ChargeType.Symbol))
		}
		div { id = "template-encircled-charge-simple"
			fieldSet("encircled-charge") { legend { +"Charge" }
				vxRemoveButton(this); br()
				vxChargeTypeLabelGen(this, "charge-type", ChargeType.Simple); br()
				vxFillLabel(this); br()
				vxChargeShapeSelectLabel(this); br()
				vxAngleLabel(this)
			}
		}
		div { id = "template-encircled-charge-symbol"
			fieldSet("encircled-charge") { legend { +"Charge" }
				vxRemoveButton(this); br()
				vxChargeTypeLabelGen(this, "charge-type", ChargeType.Symbol); br()
				vxFillLabel(this); br()
				fieldSet("stacked-charge")
			}
		}

		div { id = "template-stacked-charge-add"
			button(classes = "stacked-charge") { +"Add:"
				type = ButtonType.button
			}; vxSelectLabelGen(this, "stacked-charge-type", "",
				ChargeType.Simple, arrayOf(ChargeType.Circle, ChargeType.Simple))
		}
		div { id = "template-stacked-charge-circle"
			fieldSet("stacked-charge") { legend { +"Charge" }
				vxRemoveButton(this); br()
				vxChargeTypeLabelGen(this, "charge-type", ChargeType.Circle); br()
				label { +"Count: "
					numberInput { name = "count"
						min = 2.toString()
						max = 15.toString()
						step = 1.toString()
						value = 5.toString()
					}
				}; br()
				vxFillLabel(this); br()
				vxChargeShapeSelectLabel(this); br()
				label { +"Keep upright: "
					checkBoxInput { name = "keep-upright" }
				}; br()
				vxAngleLabel(this, "angle", "Subtended angle: ", 360); br()
				vxAngleLabel(this, labelText = "Rotation (of arc): "); br()
				fieldSet("encircled-charge")
			}
		}
		div { id = "template-stacked-charge-simple"
			fieldSet("stacked-charge") { legend { +"Charge" }
				vxRemoveButton(this); br()
				vxChargeTypeLabelGen(this, "charge-type", ChargeType.Simple); br()
				vxFillLabel(this); br()
				vxChargeShapeSelectLabel(this); br()
				vxAngleLabel(this)
			}
		}

		div { id = "template-encircled-stacked-charge-add"
			button(classes = "encircled-stacked-charge") { +"Add charge"
				type = ButtonType.button
			}; label { select { name = "encircled-stacked-charge-type"
				disabled = true
				hidden = true
				option { +ChargeType.Simple.dispString
					selected = true
					value = ChargeType.Simple.valString
				}
			} }
		}
		div { id = "template-encircled-stacked-charge-simple"
			fieldSet("encircled-stacked-charge") { legend { +"Charge" }
				vxRemoveButton(this); br()
				vxChargeTypeLabelGen(this, "charge-type", ChargeType.Simple); br()
				vxFillLabel(this); br()
				vxChargeShapeSelectLabel(this); br()
				vxAngleLabel(this)
			}
		}
	}
}

fun pointToUI() {
	UIElements.aspectSelect =
		AspectRatio.limitSelect(document.g1EBN(UIElements.aspectSelectName) as HTMLSelectElement)
	UIElements.barruletsOpts = document.gEBI(HBands.Barrulets.valString) as HTMLFieldSetElement
	UIElements.blazonTextarea = document.gEBI(UIElements.blazonTextareaID) as HTMLTextAreaElement
	UIElements.bordureWidthSelect =
		BordureWidth.limitSelect(document.g1EBN(UIElements.bordureWidthSelectName) as HTMLSelectElement)
	UIElements.cantonOpts = document.gEBI(ExtraPatternOption.Canton.valString) as HTMLFieldSetElement
	UIElements.cantonWidthSelect =
		CantonWidth.limitSelect(document.g1EBN(UIElements.cantonWidthSelectName) as HTMLSelectElement)
	UIElements.chevronWidthSelect =
		ChevronWidth.limitSelect(document.g1EBN(UIElements.chevronWidthSelectName) as HTMLSelectElement)
	UIElements.crossOpts = document.gEBI(MiscDesigns.Cross.valString) as HTMLFieldSetElement
	UIElements.crossShapeSelect =
		CrossShape.limitSelect(document.g1EBN(UIElements.crossShapeSelectName) as HTMLSelectElement)
	UIElements.designSelect =
		DesignOption.limitSelect(document.g1EBN(UIElements.designSelectName) as HTMLSelectElement)
	UIElements.exportImage = document.gEBI(UIElements.exportImageID) as HTMLImageElement
	UIElements.frameShapeSelect =
		FrameShape.limitSelect(document.g1EBN(UIElements.frameShapeSelectName) as HTMLSelectElement)
	UIElements.hoistBandWidthSelect =
		HoistBandWidth.limitSelect(document.g1EBN(UIElements.hoistBandWidthSelectName) as HTMLSelectElement)
	UIElements.orleWidthSelect =
		OrleWidth.limitSelect(document.g1EBN(UIElements.orleWidthSelectName) as HTMLSelectElement)
	UIElements.palletsOpts = document.gEBI(VBands.Pallets.valString) as HTMLFieldSetElement
	UIElements.patternSelect =
		ExtraPatternOption.limitSelect(document.g1EBN(UIElements.patternSelectName) as HTMLSelectElement)
	UIElements.paneWidthNumBox = document.g1EBN(UIElements.paneWidthNumBoxName) as HTMLInputElement
	UIElements.presetSelect =
		FlagPreset.limitSelect(document.g1EBN(UIElements.presetSelectName) as HTMLSelectElement)
	UIElements.randomiseButton = document.gEBI(UIElements.randomiseButtonID) as HTMLButtonElement
	UIElements.randomiseOnSelectCheckbox = document.g1EBN(UIElements.randomiseOnSelectCheckboxName) as HTMLInputElement
	UIElements.reverseHoistCheckbox = document.g1EBN(UIElements.reverseHoistCheckboxName) as HTMLInputElement
}

fun postPopulate() {
	for (tag in listOf("input", "select"))
		for (e in document.getElementsByTagName(tag).asList().filterNot { it.hasAttribute("disabled") }.map { it as HTMLElement })
			e.onchange = { updateView() }
	UIElements.designSelect.select.onchange = { updateDesign() }
	UIElements.paneWidthNumBox.onchange = { updatePaneWidth() }
	UIElements.presetSelect.select.onchange = null
	for (e in document.getElementsByClassName(UIElements.randomiseOptClass).asList().map { it as HTMLInputElement }) {
		e.onchange = { updateRandOpts(e.name) }
		updateRandOpts(e.name)
	}
	val chargedTemplate = document.gEBI("template-band-charged")!!
	val unchargedTemplate = document.gEBI("template-band-uncharged")!!
	val polybandElems = mutableMapOf("h" to emptyMap<Int, MutableMap<Int, HTMLElement>>().toMutableMap(),
		"v" to emptyMap<Int, MutableMap<Int, HTMLElement>>().toMutableMap())
	for ((dir, bandNames) in mapOf("h" to HBands.bandNames, "v" to VBands.bandNames)) {
		bandNames.forEachIndexed { i, iBandNames ->
			polybandElems[dir]!![i] = mutableMapOf(0 to document.gEBI(iBandNames[0])!!)
			for (j in 1..(i + 2)) {
				val cloned = ((if (i > 2) unchargedTemplate else chargedTemplate).firstElementChild as HTMLElement)
					.cloneElement()
				polybandElems[dir]!![i]!![0]!!.appendChild(cloned)
				cloned.setAttribute("id", "$dir${i + 2}band-$j")
				cloned.firstElementChild!!.innerHTML = "${iBandNames[j]} band"
			}
		}
	}
	for (j in 1..2) {
		val cloned = (chargedTemplate.firstElementChild as HTMLElement).cloneElement()
		document.gEBI("per-bend")!!.appendChild(cloned)
		cloned.setAttribute("id", "per-bend-band-$j")
		cloned.firstElementChild!!.innerHTML = "${HBands.bandNames[0][j]} band"
	}
	document.gEBI("template-encircled-charge-symbol")!!.firstElementChild!!.lastElementChild!!
		.setAttribute("class", "encircled-stacked-charge")
	repeat(2) { document.gEBI("template-stacked-charge-circle")!!.firstElementChild!!.lastElementChild!!.remove() }
	DesignOption.MiscDesigns.Field.getLookupMap().values.forEach {
		resetFieldsets(document.gEBI(it.valString) as HTMLFieldSetElement)
	}
	PatternOptElemsSpinner.patternOptElems = ExtraPatternOption.values()
		.map { document.gEBI(it.valString) as HTMLFieldSetElement }
	PatternOptElemsSpinner.patternOptElems.forEach { resetFieldsets(it) }
	randomiseRandomButton()
	RandomColourSpinner.refill()
	updatePaneWidth()
	UIElements.blazonTextarea.innerHTML = "I'll make my own lipsum, with blackjack and hookers!"
}
