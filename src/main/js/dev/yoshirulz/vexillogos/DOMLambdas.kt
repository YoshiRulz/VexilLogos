package dev.yoshirulz.vexillogos

import dev.yoshirulz.vexillogos.Canton.Companion.CantonContentType
import kotlinx.html.*

fun vxAngleLabel(parent: HtmlBlockTag, inputName: String = "rotation", labelText: String = "Rotation: ",
		defaultValue: Int = 0) =
	parent.label { +labelText
		numberInput(classes = "angle") { name = inputName
			min = 0.toString()
			max = 360.toString()
			step = 15.toString()
			value = defaultValue.toString()
		}
	}

fun vxCantonContentTypeLabelGen(parent: HtmlBlockTag, inputName: String, defaultOption: CantonContentType) =
	parent.label { +"Type: "
		select { name = inputName
			disabled = true
			option { +defaultOption.dispString
				selected = true
				value = defaultOption.valString
			}
		}
	}

fun vxChargeFieldset(parent: HtmlBlockTag) = parent.fieldSet("charge")

fun vxChargeShapeSelectLabel(parent: HtmlBlockTag, inputName: String = "charge-shape", labelText: String = "Shape: ",
		defaultOption: ChargeShape = ChargeShape.Star5) =
	vxSelectLabelGen(parent, inputName, labelText, defaultOption, groupedValues = ChargeShape.selectMap)

fun <T: ChargeType> vxChargeTypeLabelGen(parent: HtmlBlockTag, inputName: String, defaultOption: T) =
	parent.label { +"Type: "
		select { name = inputName
			disabled = true
			option { +defaultOption.selectedString
				selected = true
				value = defaultOption.valString
			}
		}
	}

fun vxFillLabel(parent: HtmlBlockTag, labelText: String = "Fill: ", classes: String? = null) =
	parent.label(classes) { +labelText; colorInput { name = "fill" } }

fun vxRemoveButton(parent: HtmlBlockTag) = parent.button { +"Remove"; type = ButtonType.button }

fun <T: SelectOption<*>> vxSelectLabelGen(parent: HtmlBlockTag, inputName: String, labelText: String, defaultOption: T,
		looseValues: Array<out T> = emptyArray(), groupedValues: Map<String, Array<out T>> = emptyMap()) =
	parent.label { +labelText
		select { name = inputName
			looseValues.forEach { option ->
				option { +option.dispString
					if (option == defaultOption) selected = true
					value = option.valString
				}
			}
			groupedValues.forEach { entry ->
				optGroup(entry.key) {
					entry.value.forEach { option ->
						option { +option.dispString
							if (option == defaultOption) selected = true
							value = option.valString
						}
					}
				}
			}
		}
	}

fun vxSingleFimbWidthLabelGen(parent: HtmlBlockTag, inputName: String, defaultOption: SingleFimbriationWidth) =
	parent.label { +"Thickness: "
		select { name = inputName
			disabled = true
			option { +defaultOption.dispString
				selected = true
				value = defaultOption.valString
			}
		}
	}
