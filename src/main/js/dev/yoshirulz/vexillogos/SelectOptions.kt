package dev.yoshirulz.vexillogos

import org.w3c.dom.HTMLSelectElement

interface RawOption {
	val dispString: String
	val valString: String
}

interface SelectOption<T: RawOption>: RawOption {
	fun getLookupMap(): Map<String, T>
}

data class OptionParser<T: RawOption>(val lookupMap: Map<String, T>)

enum class AspectRatio(h: Int, w: Int, val weight: Int): SelectOption<AspectRatio> {
	Square(1, 1, 1), TwoThree(2, 3, 3), ThreeFive(3, 5, 3), OneTwo(1, 2, 2), TenNineteen(10, 19, 0);
	companion object {
		private val PARSER = OptionParser(arrayMappedByValString(values()))
		fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
	}
	override val dispString = "$h:$w"
	override val valString = "$h-$w"
	fun writeToUI() {
		UIElements.aspectSelect.value = this
	}
	override fun getLookupMap() = PARSER.lookupMap
}

enum class FlagPreset(override val valString: String, override val dispString: String, val data: FlagData):
		SelectOption<FlagPreset> {
	EmiratiPreset("un-ae", "Flag of the UAE", FlagData.EmiratiFlag),
	AntiguanPreset("un-ag", "Flag of Antigua and Barbuda", FlagData.AntiguanFlag),
	AustralianPreset("un-au", "Flag of Australia", FlagData.AustralianFlag),
	CzechPreset("un-cz", "Flag of Czechia", FlagData.CzechFlag),
	DominiquePreset("un-dm", "Flag of Dominica", FlagData.DominiqueFlag),
	BritishPreset("un-gb", "Flag of the UK (Union Jack)", FlagData.BritishFlag),
	MontenegrinPreset("un-me", "Flag of Montenegro", FlagData.MontenegrinFlag),
	LiberianPreset("un-lr", "Flag of Liberia", FlagData.LiberianFlag),
	UkrainianPreset("un-ua", "Flag of Ukraine", FlagData.UkrainianFlag),
	UkrainianBPreset("x-ua-pres", "Flag of Ukraine w/ Tryzub in canton", FlagData.UkrainianFlagB),
	ScottishPreset("nis-gb-sct", "Flag of Scotland (St. Andrew's Cross)", FlagData.ScottishFlag);
	companion object {
		val lookupMap = arrayMappedByValString(values())
		private val PARSER = OptionParser(lookupMap)
		fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
	}
	fun writeToUI() {
		data.aspect.writeToUI()
		data.design.writeToUI()
		ExtraPatternOption.values().forEach { hideSection(it.valString, false) }
		data.extraPatterns.forEach { it.writeToUI(false) }
	}
	override fun getLookupMap() = PARSER.lookupMap
}

enum class SingleFimbriationWidth(override val valString: String, override val dispString: String):
		SelectOption<SingleFimbriationWidth> {
	Thin("thin", "Thin fimbriation"),
	Thick("thick", "Thick fimbriation");
	companion object {
		private val PARSER = OptionParser(arrayMappedByValString(values()))
		fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
	}
	override fun getLookupMap() = PARSER.lookupMap
}
