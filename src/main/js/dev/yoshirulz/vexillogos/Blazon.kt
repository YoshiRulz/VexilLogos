package dev.yoshirulz.vexillogos

interface Blazon {
	fun asEnglishString(): String
	fun asFrenchString(): String
}

typealias BlazonList = List<Blazon>
/**
 * Joins `Blazon`s with **empty strings**. Put spaces in adjacent `BlazonRawText`s.
 */
fun BlazonList.concat(isFrench: Boolean = false) =
	(if (isFrench) map { it.asFrenchString() } else map { it.asEnglishString() }).joinToString("")

data class BlazonRawText(val english: String, val french: String? = null): Blazon {
	override fun asEnglishString() = english
	override fun asFrenchString() = french ?: throw RuntimeException("oops")
	override fun toString() = asEnglishString()
}

data class BlazonNumber(private val n: Int): Blazon {
	companion object {
		private val english = arrayOf(
			"zero", "one", "two", "three", "four",
			"five", "six", "seven", "eight", "nine",
			"ten", "eleven", "twelve", "thirteen", "fourteen",
			"fifteen", "sixteen", "seventeen", "eighteen", "nineteen",
			"twenty", "twenty-one", "twenty-two", "twenty-three", "twenty-four",
			"twenty-five", "twenty-six", "twenty-seven", "twenty-eight", "twenty-nine",
			"thirty", "thirty-one", "thirty-two", "thirty-three", "thirty-four",
			"thirty-five", "thirty-six", "thirty-seven", "thirty-eight", "thirty-nine",
			"fourty", "fourty-one", "fourty-two", "fourty-three", "fourty-four",
			"fourty-five", "fourty-six", "fourty-seven", "fourty-eight", "fourty-nine",
			"fifty", "fifty-one", "fifty-two", "fifty-three", "fifty-four",
			"fifty-five", "fifty-six", "fifty-seven", "fifty-eight", "fifty-nine",
			"sixty", "sixty-one", "sixty-two", "sixty-three", "sixty-four",
			"sixty-five", "sixty-six", "sixty-seven", "sixty-eight", "sixty-nine",
			"seventy", "seventy-one", "seventy-two", "seventy-three", "seventy-four",
			"seventy-five", "seventy-six", "seventy-seven", "seventy-eight", "seventy-nine",
			"eighty", "eighty-one", "eighty-two", "eighty-three", "eighty-four",
			"eighty-five", "eighty-six", "eighty-seven", "eighty-eight", "eighty-nine",
			"ninety", "ninety-one", "ninety-two", "ninety-three", "ninety-four",
			"ninety-five", "ninety-six", "ninety-seven", "ninety-eight", "ninety-nine",
			"one hundred")
		private val french = english
		private fun construct(a: Array<String>, n: Int) = when {
			n < 0 -> throw Exception("oops")
			n < 100 -> a[n]
			else -> (n % 100).let { if (it == 0) "${a[n / 100]} hundred" else "${a[n / 100]} hundred and ${a[it]}" }
		}
	}
	override fun asEnglishString() = construct(english, n)
	override fun asFrenchString() = construct(french, n)
	override fun toString() = asEnglishString()
}
