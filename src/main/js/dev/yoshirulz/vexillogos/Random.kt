package dev.yoshirulz.vexillogos

import dev.yoshirulz.vexillogos.WeightedRandomSpinner.CustomSpinner
import dev.yoshirulz.vexillogos.WeightedRandomSpinner.SimpleSpinner
import org.w3c.dom.*
import kotlinx.dom.hasClass
import kotlin.random.Random

object AspectsSpinner: SimpleSpinner<AspectRatio>(mutableListOf<AspectRatio>().also { list ->
	AspectRatio.values().forEach { ratio -> for (i in 0 until ratio.weight) list.add(ratio) }
})

object ColourPartsSpinner: SimpleSpinner<String>(listOf("00", "1F", "2F", "3F", "4F", "5F", "6F", "7F", "8F", "9F",
	"AF", "BF", "CF", "DF", "EF", "FF"))

object DesignsSpinner: SimpleSpinner<DesignOption>(DesignOption.MiscDesigns.Field.getLookupMap().values.toList())

object DieFaceSpinner: SimpleSpinner<String>(listOf("⚀", "⚁", "⚂", "⚃", "⚄", "⚅"))

object ExtrasCountSpinner: SimpleSpinner<Int>(mutableListOf<Int>().also { list ->
	mapOf(0 to 3, 1 to 4, 2 to 2, 3 to 1).forEach { entry -> for (i in 0 until entry.value) list.add(entry.key) }
})

object PatternOptElemsSpinner: CustomSpinner<HTMLFieldSetElement>() {
	var patternOptElems = emptyList<HTMLFieldSetElement>()
	override fun getRandom() = patternOptElems[randIntLT(patternOptElems.size)]
}

object PresetDesignSpinner: SimpleSpinner<FlagPreset>(FlagPreset.DominiquePreset.getLookupMap().values.toList())

object RandomColourSpinner: CustomSpinner<String>() {
	private var colourPool = mutableListOf<String>()
	fun refill(count: Int = randomColourCount) {
		colourPool = mutableListOf()
		var num = count
		while (num > 0) randomColour().also {
			if (it !in colourPool) {
				colourPool.add(it)
				--num
			}
		}
	}
	override fun getRandom() = colourPool[randIntLT(colourPool.size)]
}

object SimpleColourSpinner: SimpleSpinner<String>(listOf(
	"CF0000", // Reds
	"FF8F00", // Oranges
	"FFDF00", // Yellows
	"007F4F", "6FBFEF", // Cyans
	"1F006F", // Blues
	"000000", "FFFFFF"))

interface WeightedRandomSpinner<T: Any> {
	fun getRandom(): T
	abstract class CustomSpinner<T: Any>: WeightedRandomSpinner<T>
	open class SimpleSpinner<T: Any>(private val opts: List<T>): WeightedRandomSpinner<T> {
		override fun getRandom() = opts[randIntLT(opts.size)]
	}
}

var limitMaxRandomColours: Boolean = false
var limitToFlagColours: Boolean = true
var randomColourCount: Int = 0
var randomiseCharges: Boolean = true
var randomiseDesign: Boolean = false
var randomiseExtras: Boolean = false
var randomiseOnSelect: Boolean = true
var randomlyReselectExtras: Boolean = true

fun randBool() = Random.nextBoolean()

fun randChanceOneIn(n: Int) = randIntLT(n) == 0

fun randIntLT(n: Int) = Random.nextInt(n)

fun randomColour() = if (limitToFlagColours) SimpleColourSpinner.getRandom()
	else ColourPartsSpinner.getRandom() + ColourPartsSpinner.getRandom() + ColourPartsSpinner.getRandom()

fun randomColourFromPool() = RandomColourSpinner.getRandom()

fun randomiseButton(element: HTMLButtonElement, parent: HTMLElement, depth: Int) =
		if (element.dataset["addSectionChildAndUpdate"] != null &&
				(element.dataset["addSectionChildAndUpdate"] != "charge" || randomiseCharges) &&
				(if (depth > 0) randChanceOneIn(3) else randBool())) {
			element.click()
			filterElemsByClass(parent.getChildList(), element.getAttribute("class")!!).first() as HTMLFieldSetElement
		} else null

fun randomiseInput(element: HTMLInputElement) = when (element.type) {
	"checkbox" -> element.checked = randBool()
	"color" -> element.setValue("#${if (limitMaxRandomColours) randomColourFromPool() else randomColour()}")
	"number" -> (if (element.hasClass("angle")) AngleNumboxWrapper(element) else NumboxWrapper(element)).randomise()
	"text" -> Unit
	else -> logNonfatalError("randomiseInput", 1, "unexpected <input>.type \"${element.type}\"")
}

fun randomiseRecursively(element: HTMLFieldSetElement, depth: Int = 0): Unit = element.getChildList().forEach {
	when (it.tagName.lowercase()) {
		"button" -> randomiseButton(it as HTMLButtonElement, element, depth + 1)?.let { subsection ->
			randomiseRecursively(subsection, 2)
		}
		"fieldset" -> randomiseRecursively(it as HTMLFieldSetElement, depth + 1)
		"label" -> it.getChildList().forEach { labelled ->
			when (labelled.tagName.lowercase()) {
				"input" -> if (!element.disabled) randomiseInput(labelled as HTMLInputElement)
				"select" -> randomiseSelect(labelled as HTMLSelectElement)
			}
		}
	}
}

fun randomiseSelect(element: HTMLSelectElement) {
	element.selectedIndex = randIntLT(element.options.asList().size)
}
