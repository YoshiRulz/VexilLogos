package dev.yoshirulz.vexillogos

import dev.yoshirulz.vexillogos.Cross.Companion.CrossFimbriation
import dev.yoshirulz.vexillogos.DesignOption.*
import org.w3c.dom.HTMLFieldSetElement
import org.w3c.dom.HTMLSelectElement
import org.w3c.dom.svg.SVGElement
import kotlinx.browser.document

interface DesignOption: SelectOption<DesignOption> {
	companion object {
		private val PARSER = OptionParser(arrayMappedByValString(MiscDesigns.values(), HBands.values(), VBands.values()))
		fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
	}
	enum class MiscDesigns(override val valString: String, override val dispString: String): DesignOption {
		Field("field", "Field"),
		Quadrisection("quadrisection", "Quadrisection"),
		PerSaltire("per-saltire", "Party per saltire"),
		Gyronny("gyronny", "Gyronny (of eight)"),
		PerBend("per-bend", "Party per bend"),
		Bend("bend", "Bend"),
		Pall("pall", "Pall"),
		Saltire("saltire", "Saltire"),
		Cross("cross", "Cross")
	}
	enum class HBands(override val valString: String, override val dispString: String): DesignOption {
		Barrulets("barrulets", "Bars/barrulets"),
		HDuoband("h-duoband", "Horizontal duoband"),
		HTriband("h-triband", "Fess / Horizontal triband"),
		HTetraband("h-tetraband", "Horizontal tetraband"),
		HPentaband("h-pentaband", "Horizontal pentaband"),
		HHexaband("h-hexaband", "Horizontal hexaband"),
		HHeptaband("h-heptaband", "Horizontal heptaband");
		companion object {
			val bandNames = arrayOf(arrayOf("h-duoband", "Upper", "Lower"),
				arrayOf("h-triband", "Upper", "Mid", "Lower"),
				arrayOf("h-tetraband", "Upper", "Upper-mid", "Lower-mid", "Lower"),
				arrayOf("h-pentaband", "Upper", "Upper-mid", "Mid", "Lower-mid", "Lower"),
				arrayOf("h-hexaband", "Upper", "Near-upper", "Upper-mid", "Lower-mid", "Near-lower", "Lower"),
				arrayOf("h-heptaband", "Upper", "Near-upper", "Upper-mid", "Mid", "Lower-mid", "Near-lower", "Lower"))
		}
	}
	enum class VBands(override val valString: String, override val dispString: String): DesignOption {
		Pallets("pallets", "Pallets"),
		VDuoband("v-duoband", "Vertical duoband"),
		VTriband("v-triband", "Pale / Vertical triband"),
		VTetraband("v-tetraband", "Vertical tetraband");
		companion object {
			val bandNames = arrayOf(arrayOf("v-duoband", "Hoist", "Fly"),
				arrayOf("v-triband", "Hoist", "Center", "Fly"),
				arrayOf("v-tetraband", "Hoist", "Near-hoist", "Near-fly", "Fly"))
		}
	}
	override fun getLookupMap() = PARSER.lookupMap
}

sealed class Design {
	companion object {
		private fun parseHNBand(count: Int) = HNBand(count,
			mutableMapOf<Int, Pair<String, Colour>>().also { map ->
				for (i in count downTo 1) map[i] = "h${count}band-$i".let {
					Pair(it, getFillFromChildrenOf(document.gEBI(it) as HTMLFieldSetElement))
				}
			},
			(document.g1EBN("h${count}band-ratio")!!.asDynamic().value as String).split("-").map { it.toInt() })
		private fun parseVNBand(count: Int) = VNBand(count,
			mutableMapOf<Int, Pair<String, Colour>>().also { map ->
				for (i in count downTo 1) map[i] = "v${count}band-$i".let {
					Pair(it, getFillFromChildrenOf(document.gEBI(it) as HTMLFieldSetElement))
				}
			},
			(document.g1EBN("v${count}band-ratio")!!.asDynamic().value as String).split("-").map { it.toInt() })
		fun parseFromUI(designOption: DesignOption) = when (designOption) {
			MiscDesigns.Field -> Field(getFillFromChildrenOf(document.gEBI("field") as HTMLFieldSetElement),
				(document.gEBI("field-charge") as HTMLFieldSetElement?)?.let { Charge.parseFromUI(it) })
			MiscDesigns.Quadrisection ->
				Quadrisection(listOf("quad-0-0", "quad-1-0", "quad-0-1", "quad-1-1").map { id ->
					Pair(getFillFromChildrenOf(document.gEBI(id) as HTMLFieldSetElement),
						(document.gEBI("$id-charge") as? HTMLFieldSetElement)?.let { Charge.parseFromUI(it) })
				})
			MiscDesigns.PerSaltire -> PerSaltire((0..3).map {
				getFillFromChildrenOf(document.gEBI("per-saltire-$it") as HTMLFieldSetElement)
			})
			MiscDesigns.Gyronny -> (document.gEBI(MiscDesigns.Gyronny.valString) as HTMLFieldSetElement).let {
				Gyronny(getFillFromChildrenOf(it),
					Colour(getPropFromChildrenOf(it, "chief-fill").uppercase()))
			}
			MiscDesigns.PerBend -> PerBend(
				getFillFromChildrenOf(document.gEBI("per-bend-band-2") as HTMLFieldSetElement),
				getFillFromChildrenOf(document.gEBI("per-bend-band-1") as HTMLFieldSetElement))
			MiscDesigns.Bend -> Bend
			MiscDesigns.Pall -> Pall
			MiscDesigns.Saltire -> (document.gEBI(MiscDesigns.Saltire.valString) as HTMLFieldSetElement).let {
				Saltire(getFillFromChildrenOf(it),
					Colour(getPropFromChildrenOf(it, "saltire-fill").uppercase()))
			}
			MiscDesigns.Cross -> Cross(UIElements.crossShapeSelect.value,
				mutableListOf<Triple<Pair<Int, Int>, String, Colour>>().also {
					for (x in 1 downTo 0) for (y in 1 downTo 0) "cross-$x-$y".let { id ->
						it.add(Triple(Pair(x, y), id,
							getFillFromChildrenOf(document.gEBI(id) as HTMLFieldSetElement)))
					}
				},
				getFillFromChildrenOf(document.gEBI("cross-stripes") as HTMLFieldSetElement),
				(document.gEBI("cross-stripes-single-fimb") as HTMLFieldSetElement?)?.let { fimbOpts ->
					CrossFimbriation(Pair(getFillFromChildrenOf(fimbOpts),
						Colour(getPropFromChildrenOf(fimbOpts, "canton-fill").uppercase())),
						SingleFimbriationWidth.Thin.getLookupMap()
							.getValue(getPropFromChildrenOf(fimbOpts, "single-fimb-type")))
				},
				document.gEBI("cross-charge")?.let { Charge.parseFromUI(it as HTMLFieldSetElement) })
			HBands.Barrulets -> Barrulets(getPropFromChildrenOf(UIElements.barruletsOpts, "count").toInt(),
				getFillFromChildrenOf(UIElements.barruletsOpts),
				Colour(getPropFromChildrenOf(UIElements.barruletsOpts, "barrulet-fill").uppercase()))
			HBands.HDuoband -> parseHNBand(2)
			HBands.HTriband -> parseHNBand(3)
			HBands.HTetraband -> parseHNBand(4)
			HBands.HPentaband -> parseHNBand(5)
			HBands.HHexaband -> parseHNBand(6)
			HBands.HHeptaband -> parseHNBand(7) // Blame Zimbabwe.
			VBands.Pallets -> Pallets(getPropFromChildrenOf(UIElements.palletsOpts, "count").toInt(),
				getFillFromChildrenOf(UIElements.palletsOpts),
				Colour(getPropFromChildrenOf(UIElements.palletsOpts, "pallet-fill").uppercase()))
			VBands.VDuoband -> parseVNBand(2)
			VBands.VTriband -> parseVNBand(3)
			VBands.VTetraband -> parseVNBand(4)
			else -> null
		}
	}
	abstract val designOption: DesignOption
	abstract fun blazon(): BlazonList
	abstract fun genSVG(size: WidthHeight): SVGElement
	abstract fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement)
	fun writeToUI() {
		UIElements.designSelect.value = designOption
		writeDataToChildrenOf(document.gEBI(designOption.valString) as HTMLFieldSetElement)
		updateDesign()
	}
}

data class Field(val fill: Colour, val charge: Charge? = null): Design() {
	override val designOption = MiscDesigns.Field
	override fun blazon() = listOf(fill)
	override fun genSVG(size: WidthHeight) = createSVGBBox("field").also { view ->
		view.appendChild(createSVGRect("field", SVGRectAttrs(fill, size.w, size.h)))
		charge?.let { view.appendChild(createSVGBBox("field-charge",
				SVGBBoxAttrs("-50 -50 100 100", 30, 30, size.w / 2 - 15, size.h / 2 - 15)))
			.appendChild(it.genSVG("field-charge"))
		}
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) {
		getPropChildOf(designOpts, "fill").getFirstAsInput().setValue(fill)
		charge?.writeToUI(designOpts)
	}
}

data class Quadrisection(val quadrants: List<Pair<Colour, Charge?>>): Design() {
	companion object {
		private data class QuadBlazon(val fill: Colour): Blazon {
			override fun asEnglishString() = fill.asEnglishString()
			override fun asFrenchString() = fill.asFrenchString()
		}
	}
	override val designOption = MiscDesigns.Quadrisection
	override fun blazon() = quadrants.map { it.first }.let { colours ->
		listOf(BlazonRawText("quarterly, "), QuadBlazon(colours[0]), BlazonRawText(", "), QuadBlazon(colours[1]),
			BlazonRawText(", "), QuadBlazon(colours[2]), BlazonRawText(", and "), QuadBlazon(colours[3]))
	}
	override fun genSVG(size: WidthHeight) = createSVGBBox("quadrisection").also { view ->
		val hw = size.w / 2
		val hh = size.h / 2
		for (x in 1 downTo 0) for (y in 1 downTo 0) {
			val id = "quad-$x-$y"
			val qData = quadrants[2 * y + x]
			view.appendChild(createSVGRect(id,
				SVGRectAttrs(qData.first, if (x == 0) hw else size.w, if (y == 0) hh else size.h)))
			qData.second?.let {
				view.appendChild(createSVGBBox("$id-charge",
						SVGBBoxAttrs("-50 -50 100 100", 30, 30,
							hw * (2 * x + 1) / 2 - 15, hh * (2 * y + 1) / 2 - 15)))
					.appendChild(it.genSVG("$id-charge"))
			}
		}
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) = quadrants.forEachIndexed { i, qData ->
		val qElem = document.gEBI("quad-${i % 2}-${i / 2}") as HTMLFieldSetElement
		getPropChildOf(qElem, "fill").getFirstAsInput().setValue(qData.first.hexStr)
		qData.second?.writeToUI(qElem)
	}
}

data class PerSaltire(val fills: List<Colour>): Design() {
	override val designOption = MiscDesigns.PerSaltire
	override fun blazon() = TODO("blazon party per saltire")
	override fun genSVG(size: WidthHeight) = createSVGBBox("per-saltire").also { view ->
		3.let { view.appendChild(createSVGRect("per-saltire-$it", SVGRectAttrs(fills[it], size.w, size.h))) }
		listOf("${size.w},${size.h}", "${size.w},0", "${size.w / 2},${size.h / 2}").forEachIndexed { i, point ->
			"per-saltire-$i".let { id -> view.appendChild(createSVGPolygon(id,
				SVGPolygonAttrs(fills[i], "0,0 $point 0,${size.h}")))
			}
		}
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) = fills.forEach {
		getPropChildOf(document.gEBI("per-saltire-$it") as HTMLFieldSetElement, "fill").getFirstAsInput()
			.setValue(it.hexStr)
	}
}

data class Gyronny(val fieldFill: Colour, val gyronFill: Colour): Design() {
	override val designOption = MiscDesigns.Gyronny
	companion object {
		private fun genGyronnyPath(size: WidthHeight, hw: Int = size.w / 2, hh: Int = size.h / 2) = PointList(
			Point(hw, hh), Point(0, 0), Point(hw, 0), Point(hw, hh), Point(size.w, 0), Point(size.w, hh),
			Point(hw, hh), Point(size.w, size.h), Point(hw, size.h), Point(hw, hh), Point(0, size.h), Point(0, hh))
	}
	override fun blazon() = listOf(BlazonRawText("gyronny of eight "), gyronFill, BlazonRawText(" and "), fieldFill)
	override fun genSVG(size: WidthHeight) = createSVGBBox("gyronny").also { view ->
		view.appendChild(createSVGRect("gyronny-field", SVGRectAttrs(fieldFill, size.w, size.h)))
		view.appendChild(createSVGPolygon("gyronny-gyrons", SVGPolygonAttrs(gyronFill, genGyronnyPath(size))))
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) {
		getPropChildOf(designOpts, "fill").getFirstAsInput().setValue(fieldFill)
		getPropChildOf(designOpts, "chief-fill").getFirstAsInput().setValue(gyronFill)
	}
}

data class PerBend(val fieldFill: Colour, val chiefFill: Colour): Design() {
	override val designOption = MiscDesigns.PerBend
	override fun blazon() = listOf(BlazonRawText("per bend "), chiefFill, BlazonRawText(" and "), fieldFill)
	override fun genSVG(size: WidthHeight) = createSVGBBox("per-bend").also { view ->
		view.appendChild(createSVGRect("per-bend-band-2", SVGRectAttrs(fieldFill, size.w, size.h)))
		view.appendChild(createSVGPolygon("per-bend-band-1", SVGPolygonAttrs(chiefFill, "0,0 ${size.w},0 0,${size.h}")))
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) {
		getPropChildOf(designOpts, "fill").getFirstAsInput().setValue(fieldFill)
		getPropChildOf(designOpts, "chief-fill").getFirstAsInput().setValue(chiefFill)
	}
}

object Bend: Design() {
	override val designOption = MiscDesigns.Bend
	override fun blazon() = TODO("blazon bend")
	override fun genSVG(size: WidthHeight) = createSVGBBox("bend").also { view ->
		TODO("draw bend")
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) = TODO("write bend data to UI")
}

object Pall: Design() {
	override val designOption = MiscDesigns.Pall
	override fun blazon() = TODO("blazon pall")
	override fun genSVG(size: WidthHeight) = createSVGBBox("pall").also { view ->
		TODO("draw pall")
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) = TODO("write pall data to UI")
}

data class Saltire(val fieldFill: Colour, val saltireFill: Colour): Design() {
	companion object {
		private fun genSaltirePathIntern(size: WidthHeight, xLen: Int, yLen: Int, wLess: Int, hLess: Int,
				hw: Int, hwLess: Int, hwMore: Int, hh: Int, hhLess: Int, hhMore: Int) = PointList(
			Point(0, yLen), Point(0, 0), Point(xLen, 0), Point(hw, hhLess),
			Point(wLess, 0), Point(size.w, 0), Point(size.w, yLen), Point(hwMore, hh),
			Point(size.w, hLess), Point(size.w, size.h), Point(wLess, size.h), Point(hw, hhMore),
			Point(xLen, size.h), Point(0, size.h), Point(0, hLess), Point(hwLess, hh)
		)
		private fun genSaltirePath(size: WidthHeight, xLen: Int, yLen: Int) = Pair(size.w / 2, size.h / 2).let { (hw, hh) ->
			genSaltirePathIntern(size, xLen, yLen,
				size.w - xLen, size.h - yLen, hw, hw - xLen, hw + xLen, hh, hh - yLen, hh + yLen)
		}
	}
	override val designOption = MiscDesigns.Saltire
	override fun blazon() = listOf(fieldFill, BlazonRawText(", a saltire "), saltireFill)
	override fun genSVG(size: WidthHeight) = createSVGBBox("saltire").also { view ->
		view.appendChild(createSVGRect("saltire-field", SVGRectAttrs(fieldFill, size.w, size.h)))
		// 2/17?
		view.appendChild(createSVGPolygon("saltire-cross",
			SVGPolygonAttrs(saltireFill, genSaltirePath(size, size.w * 2 / 17, size.h * 2 / 17))))
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) {
		getPropChildOf(designOpts, "fill").getFirstAsInput().setValue(fieldFill)
		getPropChildOf(designOpts, "saltire-fill").getFirstAsInput().setValue(saltireFill)
	}
}

data class Cross(val crossType: CrossShape, val fills: List<Triple<Pair<Int, Int>, String, Colour>>,
		val stripeFill: Colour, val fimbriation: CrossFimbriation? = null, val charge: Charge? = null): Design() {
	companion object {
		data class CrossFimbriation(val fills: Pair<Colour, Colour>, val width: SingleFimbriationWidth) {
			companion object {
				fun prepareWriteToUI(parent: HTMLFieldSetElement, width: SingleFimbriationWidth) =
					"${parent.id}-single-fimb".let { chargeOptsID ->
						var chargeOpts = document.gEBI(chargeOptsID)
						if (chargeOpts == null) {
							SingleFimbriationWidth.limitSelect(getPropChildOf(parent, "single-fimb-type")
								.getFirstAsSelect()).value = width
							addSectionChild(parent.id, "single-fimb")
							chargeOpts = document.gEBI(chargeOptsID)
						}
						chargeOpts as HTMLFieldSetElement
					}
			}
			fun writeToUI(parent: HTMLFieldSetElement) = prepareWriteToUI(parent, width).let {
				getPropChildOf(it, "fill").getFirstAsInput().setValue(fills.first)
				getPropChildOf(it, "canton-fill").getFirstAsInput().setValue(fills.second)
			}
		}
		enum class CrossShape(override val valString: String, override val dispString: String):
				SelectOption<CrossShape> {
			Nordic("nordic", "Nordic"),
			Symmetric("symmetric", "Symmetric"),
			ReverseNordic("nordic-s", "Reverse (sinister) nordic");
			companion object {
				private val PARSER = OptionParser(arrayMappedByValString(values()))
				fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
			}
			override fun getLookupMap() = PARSER.lookupMap
		}
		private fun getCrossDims(w: Int, nw: Int, h: Int, s: WidthHeight, hlh: Int = s.h - h) = PointList(
			Point(0, h), Point(w, h), Point(w, 0), Point(nw, 0), Point(nw, h), Point(s.w, h), Point(s.w, hlh),
			Point(nw, hlh), Point(nw, s.h), Point(w, s.h), Point(w, hlh), Point(0, hlh))
		private fun getCrossFimbriationDims(t: Int, w: Int, nw: Int, h: Int, s: WidthHeight, hlh: Int = s.h - h) = Pair(
			listOf(
				PointList(Point(s.w, hlh), Point(nw, hlh), Point(nw, s.h), Point(nw - t, s.h), Point(nw - t, hlh - t),
					Point(s.w, hlh - t)),
				PointList(Point(0, hlh), Point(w, hlh), Point(w, s.h), Point(w + t, s.h), Point(w + t, hlh - t),
					Point(0, hlh - t)),
				PointList(Point(s.w, h), Point(nw, h), Point(nw, 0), Point(nw - t, 0), Point(nw - t, h + t),
					Point(s.w, h + t))
			),
			listOf(
				PointList(Point(0, h), Point(w, h), Point(w, 0), Point(w + t, 0), Point(w + t, h + t), Point(0, h + t)),
				PointList(Point(nw, h), Point(s.w, h), Point(s.w, h + t), Point(nw - t, h + t)),
				PointList(Point(w, hlh), Point(w + t, hlh - t), Point(w + t, s.h), Point(w, s.h))
			))
	}
	override val designOption = MiscDesigns.Cross
	override fun blazon() = TODO("blazon cross")
	override fun genSVG(size: WidthHeight) = createSVGBBox("cross").also { view ->
		val thickness = size.h / 8
		val hHeightLessThickness = size.h / 2 - thickness
		val hWidthLessThickness = when (crossType) {
			CrossShape.Nordic -> hHeightLessThickness
			CrossShape.ReverseNordic -> size.w - hHeightLessThickness - 2 * thickness
			CrossShape.Symmetric -> size.w / 2 - thickness
		}
		val hWidthMore = when (crossType) {
			CrossShape.Nordic -> hWidthLessThickness + 2 * thickness
			CrossShape.ReverseNordic -> size.w - hHeightLessThickness
			CrossShape.Symmetric -> size.w - hWidthLessThickness
		}
		fills.forEach { view.appendChild(createSVGRect(it.second,
			SVGRectAttrs(it.third, if (it.first.first > 0) size.w else hWidthLessThickness,
				if (it.first.second > 0) size.h else hHeightLessThickness)))
		}
		view.appendChild(createSVGPolygon("cross-stripes",
			SVGPolygonAttrs(stripeFill, getCrossDims(hWidthLessThickness, hWidthMore, hHeightLessThickness, size))))
		fimbriation?.let { fimb ->
			getCrossFimbriationDims(thickness * 2 / if (fimb.width == SingleFimbriationWidth.Thin) 5 else 3,
					hWidthLessThickness, hWidthMore, hHeightLessThickness, size)
				.swapPairsWith(fimb.fills).toList().let { dimFillPairs ->
					dimFillPairs.forEachIndexed { i, pair ->
						pair.first.forEachIndexed { j, poly ->
							view.appendChild(createSVGPolygon("cross-fimb-$i-$j", SVGPolygonAttrs(pair.second, poly)))
						}
					}
				}
		}
		charge?.let { view.appendChild(createSVGBBox("cross-charge",
				SVGBBoxAttrs("-50 -50 100 100", 48, 48, hWidthMore - thickness - 24, size.h / 2 - 24)))
			.appendChild(it.genSVG("cross-charge"))
		}
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) {
		CrossShape.limitSelect(getPropChildOf(designOpts, "cross-shape").getFirstAsSelect()).value = crossType
		fills.forEach {
			getPropChildOf(document.gEBI(it.second) as HTMLFieldSetElement, "fill").getFirstAsInput()
				.setValue(it.third.hexStr)
		}
		(document.gEBI("cross-stripes") as HTMLFieldSetElement).let { stripesOpts ->
			getPropChildOf(stripesOpts, "fill").getFirstAsInput().setValue(stripeFill.hexStr)
			fimbriation?.writeToUI(stripesOpts)
		}
		charge?.writeToUI(designOpts)
	}
}

data class Barrulets(val count: Int, val fieldFill: Colour, val chiefFill: Colour): Design() {
	override val designOption = HBands.Barrulets
	override fun blazon() = listOf(BlazonRawText("barry of "), BlazonNumber(count), BlazonRawText(", "), chiefFill,
		BlazonRawText(" and "), fieldFill)
	override fun genSVG(size: WidthHeight) = createSVGBBox("barrulets").also { view ->
		for (i in count downTo 1) view.appendChild(createSVGRect("barrulets-band-$i",
			SVGRectAttrs(if (i % 2 == 0) fieldFill else chiefFill, size.w, (size.h * i / count.toDouble()).toInt())))
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) {
		getPropChildOf(designOpts, "count").getFirstAsInput().setValue(count)
		getPropChildOf(designOpts, "fill").getFirstAsInput().setValue(fieldFill.hexStr)
		getPropChildOf(designOpts, "barrulet-fill").getFirstAsInput().setValue(chiefFill.hexStr)
	}
}

data class HNBand(val count: Int, val fills: Map<Int, Pair<String, Colour>>, val bandRatio: List<Int>): Design() {
	companion object {
		interface HDuobandRatio: SelectOption<HDuobandRatio> {
			companion object {
				private val PARSER = OptionParser(
					arrayMappedByValString(Halves.values(), Thirds.values(), Quarters.values(), Fifths.values()))
				fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
			}
			enum class Halves: HDuobandRatio {
				Halved;
				override val dispString = "1:1"
				override val valString = "1-1"
			}
			enum class Thirds(a: Int, b: Int): HDuobandRatio {
				OneTwo(1, 2), TwoOne(2, 1);
				override val dispString = "$a:$b"
				override val valString = "$a-$b"
			}
			enum class Quarters(a: Int, b: Int): HDuobandRatio {
				OneThree(1, 3), ThreeOne(3, 1);
				override val dispString = "$a:$b"
				override val valString = "$a-$b"
			}
			enum class Fifths(a: Int, b: Int): HDuobandRatio {
				OneFour(1, 4), TwoThree(2, 3), ThreeTwo(3, 2), FourOne(4, 1);
				override val dispString = "$a:$b"
				override val valString = "$a-$b"
			}
			override fun getLookupMap() = PARSER.lookupMap
		}
		interface HTribandRatio: SelectOption<HTribandRatio> {
			companion object {
				private val PARSER = OptionParser(
					arrayMappedByValString(Thirds.values(), Quarters.values(), Fifths.values()))
				fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
			}
			enum class Thirds: HTribandRatio {
				Trisected;
				override val dispString = "1:1:1"
				override val valString = "1-1-1"
			}
			enum class Quarters(a: Int, b: Int, c: Int): HTribandRatio {
				OneOneTwo(1, 1, 2), OneTwoOne(1, 2, 1), TwoOneOne(2, 1, 1);
				override val dispString = "$a:$b:$c"
				override val valString = "$a-$b-$c"
			}
			enum class Fifths(a: Int, b: Int, c: Int): HTribandRatio {
				OneOneThree(1, 1, 3), OneThreeOne(1, 3, 1), ThreeOneOne(3, 1, 1),
				TwoTwoOne(2, 2, 1), TwoOneTwo(2, 1, 2), OneTwoTwo(1, 2, 2);
				override val dispString = "$a:$b:$c"
				override val valString = "$a-$b-$c"
			}
			override fun getLookupMap() = PARSER.lookupMap
		}
	}
	override val designOption = when (count) {
		2 -> HBands.HDuoband
		3 -> HBands.HTriband
		4 -> HBands.HTetraband
		5 -> HBands.HPentaband
		6 -> HBands.HHexaband
		7 -> HBands.HHeptaband
		else -> throw RuntimeException("oops")
	}
	override fun blazon() = TODO("blazon horizontal n-band")
	override fun genSVG(size: WidthHeight) = createSVGBBox("h${count}band").also { view ->
		bandRatio.map { size.h * it / bandRatio.sum() }.let { bandWidths ->
			fills.forEach {
				var height = size.h
				for (j in 1..(count - it.key)) height -= bandWidths[count - j]
				view.appendChild(createSVGRect(it.value.first, SVGRectAttrs(it.value.second, size.w, height)))
			}
		}
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) {
		if (count <= 3)
			getPropChildOf(designOpts, "h${count}band-ratio").getFirstAsSelect().value = bandRatio.joinToString("-")
		fills.forEach {
			getPropChildOf(document.gEBI(it.value.first) as HTMLFieldSetElement, "fill").getFirstAsInput()
				.setValue(it.value.second.hexStr)
		}
	}
}

data class Pallets(val count: Int, val fieldFill: Colour, val flyFill: Colour): Design() {
	override val designOption = VBands.Pallets
	override fun blazon() =
		if (count % 2 == 0) listOf(BlazonRawText("paly "), flyFill, BlazonRawText(" and "), fieldFill)
		else listOf(flyFill, BlazonRawText(", "), BlazonNumber(count / 2), BlazonRawText(" pallets "), fieldFill)
	override fun genSVG(size: WidthHeight) = createSVGBBox("pallets").also { view ->
		for (i in count downTo 1) view.appendChild(createSVGRect("pallets-band-$i",
			SVGRectAttrs(if (i % 2 == 0) fieldFill else flyFill, (size.w * i / count.toDouble()).toInt(), size.h)))
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) {
		getPropChildOf(designOpts, "count").getFirstAsInput().setValue(count)
		getPropChildOf(designOpts, "fill").getFirstAsInput().setValue(fieldFill.hexStr)
		getPropChildOf(designOpts, "pallet-fill").getFirstAsInput().setValue(flyFill.hexStr)
	}
}

data class VNBand(val count: Int, val fills: Map<Int, Pair<String, Colour>>, val bandRatio: List<Int>): Design() {
	companion object {
		interface VDuobandRatio: SelectOption<VDuobandRatio> {
			companion object {
				private val PARSER = OptionParser(
					arrayMappedByValString(Halves.values(), Thirds.values(), Quarters.values()))
				fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
			}
			enum class Halves: VDuobandRatio {
				Halved;
				override val dispString = "1:1"
				override val valString = "1-1"
			}
			enum class Thirds(a: Int, b: Int): VDuobandRatio {
				OneTwo(1, 2), TwoOne(2, 1);
				override val dispString = "$a:$b"
				override val valString = "$a-$b"
			}
			enum class Quarters(a: Int, b: Int): VDuobandRatio {
				OneThree(1, 3), ThreeOne(3, 1);
				override val dispString = "$a:$b"
				override val valString = "$a-$b"
			}
			override fun getLookupMap() = PARSER.lookupMap
		}
		interface VTribandRatio: SelectOption<VTribandRatio> {
			companion object {
				private val PARSER = OptionParser(
					arrayMappedByValString(Thirds.values(), Quarters.values(), Fifths.values()))
				fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
			}
			enum class Thirds: VTribandRatio {
				Trisected;
				override val dispString = "1:1:1"
				override val valString = "1-1-1"
			}
			enum class Quarters(a: Int, b: Int, c: Int): VTribandRatio {
				OneOneTwo(1, 1, 2), OneTwoOne(1, 2, 1), TwoOneOne(2, 1, 1);
				override val dispString = "$a:$b:$c"
				override val valString = "$a-$b-$c"
			}
			enum class Fifths(a: Int, b: Int, c: Int): VTribandRatio {
				OneOneThree(1, 1, 3), OneThreeOne(1, 3, 1), ThreeOneOne(3, 1, 1),
				TwoTwoOne(2, 2, 1), TwoOneTwo(2, 1, 2), OneTwoTwo(1, 2, 2);
				override val dispString = "$a:$b:$c"
				override val valString = "$a-$b-$c"
			}
			override fun getLookupMap() = PARSER.lookupMap
		}
	}
	override val designOption = when (count) {
		2 -> VBands.VDuoband
		3 -> VBands.VTriband
		4 -> VBands.VTetraband
		else -> throw RuntimeException("oops")
	}
	override fun blazon() = TODO("blazon vertical n-band")
	override fun genSVG(size: WidthHeight) = createSVGBBox("v${count}band").also { view ->
		bandRatio.map { size.w * it / bandRatio.sum() }.let { bandWidths ->
			fills.forEach {
				var width = size.w
				for (j in 1..(count - it.key)) width -= bandWidths[count - j]
				view.appendChild(createSVGRect(it.value.first, SVGRectAttrs(it.value.second, width, size.h)))
			}
		}
	}
	override fun writeDataToChildrenOf(designOpts: HTMLFieldSetElement) = fills.forEach {
		getPropChildOf(document.gEBI(it.value.first) as HTMLFieldSetElement, "fill").getFirstAsInput()
			.setValue(it.value.second.hexStr)
	}
}
