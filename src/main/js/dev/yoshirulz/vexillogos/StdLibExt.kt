package dev.yoshirulz.vexillogos

import org.w3c.dom.*
import org.w3c.dom.svg.SVGElement
import kotlin.math.roundToInt
import kotlin.random.Random

data class Colour(val hexStr: String): Blazon {
	companion object {
		val BLACK = Colour("#000000")
		val WHITE = Colour("#FFFFFF")
	}
	override fun asEnglishString() = hexStr
	override fun asFrenchString() = hexStr
	override fun toString() = asEnglishString()
}

data class DoublePoint(val x: Double, val y: Double): PointInterface {
	override fun toString() = "${x.toFixed(2)},${y.toFixed(2)}"
}

data class MutableDoublePoint(var x: Double, var y: Double): PointInterface {
	override fun toString() = "${x.toFixed(2)},${y.toFixed(2)}"
}

data class EnumLimitedSelect<T: SelectOption<*>>(val select: HTMLSelectElement,
		private val parser: OptionParser<T>? = null) {
	var value: T
		get() = parser?.lookupMap?.get(select.value) ?: throw RuntimeException("oops")
		set(newValue) {
			select.setAttribute("value", newValue.valString)
			select.value = newValue.valString
		}
}

open class NumboxWrapper(open val input: HTMLInputElement) {
	private val max: Double get() = input.max.toDouble()
	private val min: Double get() = input.min.toDouble()
	private val step: Double get() = input.step.toDouble()
	var value: Double
		get() = input.value.toDouble()
		set(newValue) {
			input.value = newValue.toString()
		}
	open fun randomise() {
		value = (min + randIntLT(((max - min) / step).roundToInt()) * step).roundToInt().toDouble()
	}
}

data class AngleNumboxWrapper(override val input: HTMLInputElement): NumboxWrapper(input) {
	override fun randomise() {
		@Suppress("CascadeIf")
		value = (if (randBool()) 90 * randIntLT(4)
			else if (randBool()) 45 * randIntLT(8) else 15 * randIntLT(24)).toDouble()
	}
}

interface PointInterface

data class Point(val x: Int, val y: Int): PointInterface {
	override fun toString() = "$x,$y"
}

data class PointList<T: PointInterface>(val points: List<T>) {
	constructor(vararg points: T): this(listOf(*points))
	override fun toString() = points.joinToString(" ")
}

data class WidthHeight(val w: Int, val h: Int)

fun Document.g1EBN(elementName: String) = getElementsByName(elementName)[0] as HTMLElement?
fun Document.gEBI(elementId: String) = getElementById(elementId) as HTMLElement?

fun Double.toFixed(p: Int) = asDynamic().toFixed(p) as String

fun HTMLElement.cloneElement(deep: Boolean = true) = cloneNode(deep) as HTMLElement
fun HTMLElement.getFirstAsInput() = firstElementChild as HTMLInputElement
fun HTMLElement.getFirstAsSelect() = firstElementChild as HTMLSelectElement

fun HTMLInputElement.setValue(value: String) {
	setAttribute("value", value)
	this.value = value
}
fun HTMLInputElement.setValue(value: Colour) = setValue(value.hexStr)
fun HTMLInputElement.setValue(value: Int) = setValue(value.toString())

fun <A: Any?, B: Any?, X: Any?, Y: Any?> Pair<A, B>.swapPairsWith(other: Pair<X, Y>) =
	Pair(Pair(first, other.first), Pair(second, other.second))

fun ParentNode.getChildList() = children.asList().map { it as HTMLElement }

fun SVGElement.setAttributeNS(namespace: String?, qualifiedName: String, value: Colour) =
	setAttributeNS(namespace, qualifiedName, value.hexStr)
fun SVGElement.setAttributeNS(namespace: String?, qualifiedName: String, value: Int) =
	setAttributeNS(namespace, qualifiedName, value.toString())

external fun encodeURI(s: String): String
