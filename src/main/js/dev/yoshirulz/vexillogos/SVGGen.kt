package dev.yoshirulz.vexillogos

import org.w3c.dom.svg.*
import kotlinx.browser.document

const val SVG_NS = "http://www.w3.org/2000/svg"
const val XML_NS = "http://www.w3.org/2000/xmlns/"

fun createSVGElem(tag: String, title: String) = (document.createElementNS(SVG_NS, tag) as SVGElement).also {
	it.appendChild(document.createElementNS(SVG_NS, "title").also { titleElem -> titleElem.innerHTML = title })
}

data class SVGBBoxAttrs(val viewBox: String, val width: Int, val height: Int, val x: Int = 0, val y: Int = 0) {
	fun applyTo(element: SVGElement) {
		element.setAttributeNS(null, "viewBox", viewBox)
		element.setAttributeNS(null, "width", width)
		element.setAttributeNS(null, "height", height)
		element.setAttributeNS(null, "x", x)
		element.setAttributeNS(null, "y", y)
	}
}
fun createSVGBBox(titlePrefix: String, attrs: SVGBBoxAttrs? = null) =
	(createSVGElem("svg", "$titlePrefix-bbox") as SVGSVGElement).also { attrs?.applyTo(it) }
fun createPatternSVGBBox(pattern: String, size: WidthHeight) =
	createSVGBBox(pattern, SVGBBoxAttrs("0 0 ${size.w} ${size.h}", size.w, size.h))

data class SVGCircleAttrs(val fill: Colour, val r: Int, val cx: Int = 0, val cy: Int = 0)
fun createSVGCircle(title: String, attrs: SVGCircleAttrs? = null) =
	(createSVGElem("circle", title) as SVGCircleElement).also { e ->
		attrs?.let {
			e.setAttributeNS(null, "fill", it.fill)
			e.setAttributeNS(null, "r", it.r)
			e.setAttributeNS(null, "cx", it.cx)
			e.setAttributeNS(null, "cy", it.cy)
		}
	}

data class SVGPathAttrs(val fill: Colour, val d: String)
fun createSVGPath(title: String, attrs: SVGPathAttrs? = null) =
	(createSVGElem("path", title) as SVGPathElement).also { e ->
		attrs?.let {
			e.setAttributeNS(null, "fill", it.fill)
			e.setAttributeNS(null, "d", it.d)
		}
	}

data class SVGPolygonAttrs(val fill: Colour, val points: String) {
	constructor(fill: Colour, points: PointList<*>): this(fill, points.toString())
}
fun createSVGPolygon(title: String, attrs: SVGPolygonAttrs? = null) =
	(createSVGElem("polygon", title) as SVGPolygonElement).also { e ->
		attrs?.let {
			e.setAttributeNS(null, "fill", it.fill)
			e.setAttributeNS(null, "points", it.points)
		}
	}

data class SVGRectAttrs(val fill: Colour, val width: String, val height: String, val x: Int = 0, val y: Int = 0) {
	constructor(fill: Colour, width: Int, height: Int, x: Int = 0, y: Int = 0):
		this(fill, width.toString(), height.toString(), x, y)
}
fun createSVGRect(title: String, attrs: SVGRectAttrs? = null) =
	(createSVGElem("rect", title) as SVGRectElement).also { e ->
		attrs?.let {
			e.setAttributeNS(null, "fill", it.fill)
			e.setAttributeNS(null, "width", it.width)
			e.setAttributeNS(null, "height", it.height)
			e.setAttributeNS(null, "x", it.x)
			e.setAttributeNS(null, "y", it.y)
		}
	}
