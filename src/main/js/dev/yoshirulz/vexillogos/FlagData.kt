package dev.yoshirulz.vexillogos

import dev.yoshirulz.vexillogos.Canton.Companion.CantonWidth
import dev.yoshirulz.vexillogos.Cross.Companion.CrossFimbriation
import dev.yoshirulz.vexillogos.Cross.Companion.CrossShape

class FlagData(val aspect: AspectRatio, val design: Design, vararg val extraPatterns: ExtraPattern) {
	companion object {
		val AntiguanFlag = FlagData(
			AspectRatio.TwoThree,
			HNBand(
				3,
				mapOf(1 to Pair("h3band-1", Colour.BLACK),
					2 to Pair("h3band-2", Colour("#0073C7")),
					3 to Pair("h3band-3", Colour.WHITE)),
				listOf(2, 1, 2)),
			Frame(Colour("#CF0821"), Frame.Companion.FrameShape.InvIsosceles))
		val AustralianFlag = FlagData(
			AspectRatio.OneTwo,
			Quadrisection(Colour("#00247D").let { blue ->
				(0..3).map { Pair(blue, if (it == 2) FreeSimpleCharge(Colour.WHITE, ChargeShape.Star7, 0) else null) }
			}),
			Canton(DesignOption.MiscDesigns.Quadrisection, false, CantonWidth.Half.valString, Canton.Companion.CantonFlagContent("un-gb")))
		val BritishFlag = FlagData(
			AspectRatio.OneTwo,
			Saltire(Colour("#00247D"), Colour.WHITE),
			CustomFrames(Colour("#CF142B").let { red ->
				mapOf(0 to Pair(Colour.WHITE, "M83,0 h34 v34 h83 v32 h-83 v34 h-34 v-34 h-83 v-32 h83 Z"),
					1 to Pair(red, "M90,0 h20 v40 h90 v20 h-90 v40 h-20 v-40 h-90 v-20 h90 Z"),
					2 to Pair(red, "M0,0 l67,34 h-14 L0,7 Z M200,100 l-67,-34 h14 L200,93 Z"),
					3 to Pair(red, "M200,0 l-67,34 h-14 L186,0 Z M0,100 l67,-34 h14 L14,100 Z"))
			}))
		val CzechFlag = FlagData(
			AspectRatio.TwoThree,
			HNBand(
				2,
				mapOf(1 to Pair("h2band-1", Colour.WHITE),
					2 to Pair("h2band-2", Colour("#D80C13"))),
				listOf(1, 1)),
			Chevron(Chevron.Companion.ChevronWidth.Half, Colour("#08437F")))
		val DominiqueFlag = FlagData(
			AspectRatio.OneTwo,
			Cross(
				CrossShape.Symmetric,
				listOf(Pair(0, 0), Pair(0, 1), Pair(1, 0), Pair(1, 1))
					.map { Triple(it, "cross-${it.first}-${it.second}", Colour("#007641")) },
				Colour.BLACK,
				CrossFimbriation(Pair(Colour.WHITE, Colour("#F6E400")), SingleFimbriationWidth.Thick),
				FreeSymbolCharge(Colour("#D5162C"))))
		val EmiratiFlag = FlagData(
			AspectRatio.OneTwo,
			HNBand(
				3,
				mapOf(1 to Pair("h3band-1", Colour("#00742B")),
					2 to Pair("h3band-2", Colour.WHITE),
					3 to Pair("h3band-3", Colour.BLACK)),
				listOf(1, 1, 1)),
			HoistBand(Colour("#FF0000"), HoistBand.Companion.HoistBandWidth.Quarter))
		val LiberianFlag = FlagData(
			AspectRatio.TenNineteen,
			Barrulets(11, Colour.WHITE, Colour("#BF0A30")),
			Canton(DesignOption.HBands.Barrulets, false, CantonWidth.Quarter.valString, Canton.Companion.CantonFlagContent("un-gb")))
		val MontenegrinFlag = FlagData(
			AspectRatio.OneTwo,
			Field(Colour("#C40308")),
			Bordure(Colour("#D3AE3B"), Bordure.Companion.BordureWidth.Thin))
		val ScottishFlag = FlagData(AspectRatio.ThreeFive, Saltire(Colour("#0065BD"), Colour.WHITE))
		val UkrainianFlag = FlagData(
			AspectRatio.TwoThree,
			HNBand(
				2,
				mapOf(1 to Pair("h2band-1", Colour("#0057B7")),
					2 to Pair("h2band-2", Colour("#FFD700"))),
				listOf(1, 1)))
		val UkrainianFlagB = FlagData(
			AspectRatio.TwoThree,
			HNBand(
				2,
				mapOf(1 to Pair("h2band-1", Colour("#0057B7")),
					2 to Pair("h2band-2", Colour("#FFD700"))),
				listOf(1, 1)),
			Canton(
				DesignOption.HBands.HDuoband,
				false,
				CantonWidth.Quarter.valString,
				Canton.Companion.CantonChargeContent(FreeSimpleCharge(Colour("#FFD700"), ChargeShape.Tryzub, 0))))
	}
}