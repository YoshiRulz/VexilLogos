package dev.yoshirulz.vexillogos

import org.w3c.dom.HTMLFieldSetElement
import org.w3c.dom.HTMLSelectElement
import org.w3c.dom.svg.SVGElement
import kotlinx.browser.document
import org.w3c.dom.svg.SVGGElement
import kotlin.math.roundToInt

private const val TRYZUB_PATH_A = "M5.985561 78.82382a104.079383 104.079383 0 0 0 14.053598 56.017033 55 55 0 0 1-13.218774 70.637179A20 20 0 0 0 0 212.5a20 20 0 0 0-6.820384-7.021968 55 55 0 0 1-13.218774-70.637179A104.079383 104.079383 0 0 0-5.98556 78.82382l-1.599642-45.260519A30.103986 30.103986 0 0 1 0 12.5a30.103986 30.103986 0 0 1 7.585202 21.063301zM5 193.624749a45 45 0 0 0 6.395675-53.75496A114.079383 114.079383 0 0 1 0 112.734179a114.079383 114.079383 0 0 1-11.395675 27.13561A45 45 0 0 0-5 193.624749V162.5H5z"

private const val TRYZUB_PATH_B = "M27.779818 75.17546A62.64982 62.64982 0 0 1 60 27.5v145H0l-5-10a22.5 22.5 0 0 1 17.560976-21.95122l14.634147-3.292683a10 10 0 1 0-4.427443-19.503751zm5.998315 34.353887a20 20 0 0 1-4.387889 37.482848l-14.634146 3.292683A12.5 12.5 0 0 0 5 162.5h45V48.265462a52.64982 52.64982 0 0 0-12.283879 28.037802zM42 122.5h10v10H42z"

enum class ChargeType(override val valString: String, override val dispString: String, val selectedString: String):
		SelectOption<ChargeType> {
	Circle("circle", "Circle/arc of charges", "Circle/arc (of identical symbols)"),
	Simple("simple", "Simple symbol", "Symbol (simple)"),
	Symbol("symbol", "Stacked/custom symbol", "Symbol (advanced)");
	companion object {
		private val PARSER = OptionParser(arrayMappedByValString(ChargeType.values()))
		fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
		fun limitTypeFromOpts(chargeOpts: HTMLFieldSetElement) =
			ChargeType.limitSelect(getPropChildOf(chargeOpts, "charge-type").getFirstAsSelect())
	}
	override fun getLookupMap() = PARSER.lookupMap
}

enum class ChargeShape(override val valString: String, override val dispString: String): SelectOption<ChargeShape> {
	Star3("star-3", "Star (3-pointed)"),
	Star4("star-4", "Star (4-pointed)"),
	Star5("star-5", "Star (5-pointed)"),
	Pentagram("star-pentagram", "Pentagram"),
	Star6("star-6", "Star (6-pointed)"),
	Hexagram("star-hexagram", "Hexagram (Star of David)"),
	Star7("star-7", "Star (7-pointed)"),
	Lozenge("lozenge", "Lozenge"),
	FleurDeLis("fleur-de-lis", "Fleur-de-lis"),
	Rose("rose", "Rose"),
	Thistle("thistle", "Thistle"),
	Shamrock("shamrock", "Shamrock (three-leaf clover)"),
	FourLeafClover("4-leaf-clover", "Four-leaf clover"),
	Wattle("wattle", "Wattle"),
	Anchor("anchor", "Anchor"),
	SunOfMay("sun-of-may", "Sun of May"),
	SunInSplendour("sol", "Sun (in his splendour)"),
	MoonInPlenitude("luna", "Moon (in her plenitude)"),
	HammerAndSickle("hammer-and-sickle", "Hammer and sickle"),
	Trident("trident", "Trident"),
	Tryzub("tryzub", "Tryzub (Ukrainian trident)"),
	MapleLeaf("maple-leaf", "Maple leaf"),
	Snowflake("snowflake", "Snowflake"),
	Cross("cross-ordinary", "(Latin) Cross"),
	RussianOrthodoxCross("russian-orthodox-cross", "Russian Orthodox cross"),
	CrossBottony("cross-bottony", "Cross bottony"),
	CrossCrosslet("cross-crosslet", "Cross crosslet"),
	CrossFlory("cross-flory", "Cross flory"),
	JerusalemCross("jerusalem-cross", "Jerusalem cross"),
	MalteseCross("maltese-cross", "Maltese cross"),
	CrossMoline("cross-moline", "Cross moline"),
	CrossPatee("cross-patee", "Cross patée"),
	CrossPatonce("cross-patonce", "Cross patonce"),
	CrossPotent("cross-potent", "Cross potent"),
	OccitanCross("occitan-cross", "Occitan Cross"),
	Triquetra("triquetra", "Triquetra"),
	StarAndCrescent("star-and-crescent", "Star and crescent"),
	Dharmachakra("dharmachakra", "Wheel of dharma"),
	Taijitu("taijitu", "Taijitu (yin-yang symbol)");
	companion object {
		private val PARSER = OptionParser(arrayMappedByValString(ChargeShape.values()))
		val selectMap = mapOf(
			"Stars" to arrayOf(Star3, Star4, Star5, Pentagram, Star6, Hexagram, Star7),
			"Crosses" to arrayOf(Cross, RussianOrthodoxCross, CrossBottony, CrossCrosslet, CrossFlory, JerusalemCross,
				MalteseCross, CrossMoline, CrossPatee, CrossPatonce, CrossPotent, OccitanCross),
			"Celestial" to arrayOf(SunOfMay, SunInSplendour, MoonInPlenitude, StarAndCrescent),
			"Flora" to arrayOf(FleurDeLis, Rose, Thistle, Shamrock, FourLeafClover, Wattle, MapleLeaf),
			"Misc. Abstract" to arrayOf(Lozenge, Triquetra, Taijitu),
			"Misc." to arrayOf(Anchor, HammerAndSickle, Trident, Tryzub, Snowflake, Dharmachakra))
		fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
	}
	override fun getLookupMap() = PARSER.lookupMap
}

interface Charge {
	companion object {
		private class InvalidChargeTypeException(type: ChargeType): RuntimeException("Invalid charge type \"$type\"")
		private fun parseCircleCharge(chargeOpts: HTMLFieldSetElement) = StackedCircleCharge(
			getPropFromChildrenOf(chargeOpts, "count").toInt(),
			Colour(getPropFromChildrenOf(chargeOpts, "fill")),
			ChargeShape.limitSelect(getPropChildOf(chargeOpts, "charge-shape").getFirstAsSelect()).value,
			getPropChildOf(chargeOpts, "keep-upright").getFirstAsInput().checked,
			getPropFromChildrenOf(chargeOpts, "angle").toInt(),
			getPropFromChildrenOf(chargeOpts, "rotation").toInt())
		private fun parseSimpleCharge(chargeOpts: HTMLFieldSetElement) = FreeSimpleCharge(
			Colour(getPropFromChildrenOf(chargeOpts, "fill")),
			ChargeShape.limitSelect(getPropChildOf(chargeOpts, "charge-shape").getFirstAsSelect()).value,
			getPropFromChildrenOf(chargeOpts, "rotation").toInt())
		private fun parseEncircledCharge(chargeOpts: HTMLFieldSetElement) =
			when (val type = ChargeType.limitTypeFromOpts(chargeOpts).value) {
				ChargeType.Simple -> EncircledSimpleCharge(parseSimpleCharge(chargeOpts))
				ChargeType.Symbol -> EncircledSymbolCharge(
					Colour(getPropFromChildrenOf(chargeOpts, "fill")),
					(chargeOpts.getChildList().first { it.classList.contains("encircled-stacked-charge") }
						as? HTMLFieldSetElement)?.let { parseStackedCharge(it) })
				else -> throw InvalidChargeTypeException(type)
			}
		private fun parseStackedCharge(chargeOpts: HTMLFieldSetElement) =
			when (val type = ChargeType.limitTypeFromOpts(chargeOpts).value) {
				ChargeType.Circle -> parseCircleCharge(chargeOpts)
				ChargeType.Simple -> StackedSimpleCharge(parseSimpleCharge(chargeOpts))
				else -> throw InvalidChargeTypeException(type)
			}
		fun parseFromUI(chargeOpts: HTMLFieldSetElement) = when (chargeOpts.className) {
			"charge" -> when (ChargeType.limitTypeFromOpts(chargeOpts).value) {
				ChargeType.Circle -> FreeCircleCharge(
					parseCircleCharge(chargeOpts),
					(chargeOpts.getChildList().first { it.classList.contains("encircled-charge") }
						as? HTMLFieldSetElement)?.let { parseEncircledCharge(it) })
				ChargeType.Simple -> parseSimpleCharge(chargeOpts)
				ChargeType.Symbol -> FreeSymbolCharge(
					Colour(getPropFromChildrenOf(chargeOpts, "fill")),
					(chargeOpts.getChildList().first { it.classList.contains("stacked-charge") }
						as? HTMLFieldSetElement)?.let { parseStackedCharge(it) })
			}
			"encircled-charge" -> parseEncircledCharge(chargeOpts)
			"stacked-charge" -> parseStackedCharge(chargeOpts)
			"encircled-stacked-charge" -> when (val type = ChargeType.limitTypeFromOpts(chargeOpts).value) {
				ChargeType.Simple -> EncircledStackedSimpleCharge(parseSimpleCharge(chargeOpts))
				else -> throw InvalidChargeTypeException(type)
			}
			else -> throw RuntimeException("Invalid charge class \"${chargeOpts.className}\"")
		}
		fun prepareWriteToUI(parent: HTMLFieldSetElement, type: ChargeType, chargeClass: String) =
			"${parent.id}-$chargeClass".let { chargeOptsID ->
				document.gEBI(chargeOptsID) ?: let {
					ChargeType.limitSelect(getPropChildOf(parent, "charge-type").getFirstAsSelect()).value = type
					addSectionChild(parent.id, chargeClass)
					document.gEBI(chargeOptsID)!!
				}
			} as HTMLFieldSetElement
	}
	val chargeClass: String
	val chargeType: ChargeType
	fun genSVG(title: String): SVGElement
	fun writeDataToChildrenOf(chargeOpts: HTMLFieldSetElement)
	fun writeToUI(parent: HTMLFieldSetElement) =
		writeDataToChildrenOf(prepareWriteToUI(parent, chargeType, chargeClass))
}
interface CircleCharge: Charge {
	override val chargeType get() = ChargeType.Circle
	val count: Int
	val fill: Colour
	val keepUpright: Boolean
	val rotation: Int
	val shape: ChargeShape
	val subAngle: Int
	override fun genSVG(title: String) = createSVGCircle("not-a-circle")
	override fun writeDataToChildrenOf(chargeOpts: HTMLFieldSetElement) {
		getPropChildOf(chargeOpts, "count").getFirstAsInput().setValue(count)
		getPropChildOf(chargeOpts, "fill").getFirstAsInput().setValue(fill)
		ChargeShape.limitSelect(getPropChildOf(chargeOpts, "charge-shape").getFirstAsSelect()).value = shape
		getPropChildOf(chargeOpts, "keep-upright").getFirstAsInput().checked = keepUpright
		getPropChildOf(chargeOpts, "angle").getFirstAsInput().setValue(subAngle)
		getPropChildOf(chargeOpts, "rotation").getFirstAsInput().setValue(rotation)
	}
}
interface SimpleCharge: Charge {
	companion object {
		private fun genSimpleStarPoints(count: Int, inRad: Int, circumRad: Int) = PointList((1..2 * count)
			.map { polarToCart(if (it % 2 == 0) circumRad else inRad, it.toDouble() / count) }.toTypedArray().asList())
	}
	override val chargeType get() = ChargeType.Simple
	val fill: Colour
	val rotation: Int
	val shape: ChargeShape
	private fun genStarWithNPoints(n: Int, hWidth: Int, title: String) = createSVGPolygon(title,
			SVGPolygonAttrs(fill, genSimpleStarPoints(n, (hWidth * 0.4).roundToInt(), hWidth).toString()))
		.also { it.setAttributeNS(null, "transform", "rotate(${rotation - 90})") }
	override fun genSVG(title: String) = 50.let { hWidth ->
		when (shape) {
			ChargeShape.Star3 -> genStarWithNPoints(3, hWidth, title)
			ChargeShape.Star4 -> genStarWithNPoints(4, hWidth, title)
			ChargeShape.Star5 -> genStarWithNPoints(5, hWidth, title)
			ChargeShape.Star6 -> genStarWithNPoints(6, hWidth, title)
			ChargeShape.Star7 -> genStarWithNPoints(7, hWidth, title)
			ChargeShape.Tryzub -> (createSVGElem("g", "$title-group") as SVGGElement).also { e ->
				e.setAttributeNS(null, "transform", "scale(${hWidth / 115.25}) translate(0, -115.25)")
				e.appendChild(createSVGPath("$title-1", SVGPathAttrs(fill, TRYZUB_PATH_A)))
				e.appendChild(createSVGPath("$title-2", SVGPathAttrs(fill, TRYZUB_PATH_B)))
				e.appendChild(createSVGPath("$title-3", SVGPathAttrs(fill, TRYZUB_PATH_B)).also {
					it.setAttributeNS(null, "transform", "scale(-1 1)")
				})
			}
			else -> createSVGCircle(title, SVGCircleAttrs(fill, hWidth))
		}
	}
	override fun writeDataToChildrenOf(chargeOpts: HTMLFieldSetElement) {
		getPropChildOf(chargeOpts, "fill").getFirstAsInput().setValue(fill)
		ChargeShape.limitSelect(getPropChildOf(chargeOpts, "charge-shape").getFirstAsSelect()).value = shape
		getPropChildOf(chargeOpts, "rotation").getFirstAsInput().setValue(rotation)
	}
}
interface SymbolCharge: Charge {
	override val chargeType get() = ChargeType.Symbol
	val fill: Colour
	val stackedCharge: StackedCharge?
	override fun genSVG(title: String) = createSVGCircle(title, SVGCircleAttrs(fill, 50))
	override fun writeDataToChildrenOf(chargeOpts: HTMLFieldSetElement) {
		getPropChildOf(chargeOpts, "fill").getFirstAsInput().setValue(fill)
		stackedCharge?.writeToUI(chargeOpts)
	}
}
interface FreeCharge: Charge {
	override val chargeClass get() = "charge"
}
interface EncircledCharge: Charge {
	override val chargeClass get() = "encircled-charge"
}
interface StackedCharge: Charge {
	override val chargeClass get() = "stacked-charge"
}
data class FreeCircleCharge(override val count: Int, override val fill: Colour, override val shape: ChargeShape,
		override val keepUpright: Boolean, override val subAngle: Int, override val rotation: Int,
		val encircledCharge: EncircledCharge? = null): FreeCharge, CircleCharge {
	constructor(data: StackedCircleCharge, encircledCharge: EncircledCharge?):
		this(data.count, data.fill, data.shape, data.keepUpright, data.subAngle, data.rotation, encircledCharge)
}
data class StackedCircleCharge(override val count: Int, override val fill: Colour, override val shape: ChargeShape,
		override val keepUpright: Boolean, override val subAngle: Int, override val rotation: Int):
	StackedCharge, CircleCharge
data class FreeSimpleCharge(override val fill: Colour, override val shape: ChargeShape, override val rotation: Int):
	FreeCharge, SimpleCharge
data class EncircledSimpleCharge(override val fill: Colour, override val shape: ChargeShape,
		override val rotation: Int): EncircledCharge, SimpleCharge {
	constructor(data: FreeSimpleCharge): this(data.fill, data.shape, data.rotation)
}
data class StackedSimpleCharge(override val fill: Colour, override val shape: ChargeShape, override val rotation: Int):
		StackedCharge, SimpleCharge {
	constructor(data: FreeSimpleCharge): this(data.fill, data.shape, data.rotation)
}
data class EncircledStackedSimpleCharge(override val fill: Colour, override val shape: ChargeShape,
		override val rotation: Int): SimpleCharge {
	constructor(data: FreeSimpleCharge): this(data.fill, data.shape, data.rotation)
	override val chargeClass = "encircled-stacked-charge"
}
data class FreeSymbolCharge(override val fill: Colour, override val stackedCharge: StackedCharge? = null):
	FreeCharge, SymbolCharge
data class EncircledSymbolCharge(override val fill: Colour, override val stackedCharge: StackedCharge? = null):
	EncircledCharge, SymbolCharge
