package dev.yoshirulz.vexillogos

import dev.yoshirulz.vexillogos.Chevron.Companion.ChevronFimbriation
import dev.yoshirulz.vexillogos.DesignOption.VBands
import dev.yoshirulz.vexillogos.Frame.Companion.FrameShape
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLFieldSetElement
import org.w3c.dom.HTMLSelectElement
import org.w3c.dom.get
import org.w3c.dom.svg.SVGElement
import kotlinx.browser.document
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.set

enum class ExtraPatternOption(override val valString: String, override val dispString: String):
		SelectOption<ExtraPatternOption> {
	Canton("canton", "Canton"),
	HoistBand("hoist-band", "Hoist band"),
	Chevron("chevron", "Chevron"),
	Frame("frame", "Frame"),
	Bordure("bordure", "Bordure"),
	Orle("orle", "Orle"),
	CustomFrame("custom-frames", "Custom frame(s)");
	companion object {
		private val PARSER = OptionParser(arrayMappedByValString(ExtraPatternOption.values()))
		fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
	}
	override fun getLookupMap() = PARSER.lookupMap
}

sealed class ExtraPattern(private val patternOption: ExtraPatternOption) {
	companion object {
		fun parseFromUI(patternOption: ExtraPatternOption, currentDesign: DesignOption) = when (patternOption) {
			ExtraPatternOption.Canton -> Canton(currentDesign, isHoistReversed,
				UIElements.cantonWidthSelect.value.valString,
				(document.gEBI("canton-canton-content") as HTMLFieldSetElement?)?.let {
					when (Canton.Companion.CantonContentType.limitSelect(document.g1EBN("canton-content-type") as HTMLSelectElement).value) {
						Canton.Companion.CantonContentType.Charge -> Canton.Companion.CantonChargeContent((document.gEBI("canton-canton-content-charge") as HTMLFieldSetElement?)?.let(Charge::parseFromUI))
						Canton.Companion.CantonContentType.Flag -> Canton.Companion.CantonFlagContent(getPropFromChildrenOf(it, "flag"))
					}
				})
			ExtraPatternOption.HoistBand -> HoistBand(
				getFillFromChildrenOf(document.gEBI("hoist-band") as HTMLFieldSetElement),
				UIElements.hoistBandWidthSelect.value)
			ExtraPatternOption.Chevron -> Chevron(UIElements.chevronWidthSelect.value,
				getFillFromChildrenOf(document.gEBI("chevron") as HTMLFieldSetElement),
				(document.gEBI("chevron-single-fimb") as HTMLFieldSetElement?)?.let { fimbOpts ->
					ChevronFimbriation(
                        getFillFromChildrenOf(fimbOpts),
                        SingleFimbriationWidth.Thin.getLookupMap()
                            .getValue(getPropFromChildrenOf(fimbOpts, "single-fimb-type")))
				})
			ExtraPatternOption.Frame -> Frame(getFillFromChildrenOf(document.gEBI("frame") as HTMLFieldSetElement),
				FrameShape.limitSelect(document.g1EBN(UIElements.frameShapeSelectName) as HTMLSelectElement).value)
			ExtraPatternOption.Bordure -> Bordure(
				getFillFromChildrenOf(document.gEBI("bordure") as HTMLFieldSetElement),
				UIElements.bordureWidthSelect.value)
			ExtraPatternOption.Orle -> Orle(UIElements.orleWidthSelect.value,
				getFillFromChildrenOf(document.gEBI("orle") as HTMLFieldSetElement))
			ExtraPatternOption.CustomFrame -> CustomFrames(customFrames.associateWith { i ->
				(document.gEBI("custom-frame-$i") as HTMLFieldSetElement).let {
					Pair(getFillFromChildrenOf(it), getPropFromChildrenOf(it, "path"))
				}
			})
		}
	}
	abstract fun genSVG(size: WidthHeight): SVGElement
	abstract fun writeDataToChildrenOf(patternOpts: HTMLFieldSetElement)
	fun writeToUI(triggerUpdate: Boolean = true) {
		showPattern(document.gEBI(patternOption.valString) as HTMLFieldSetElement)
		writeDataToChildrenOf(document.gEBI(patternOption.valString) as HTMLFieldSetElement)
		if (triggerUpdate) updateView()
	}
}

data class Bordure(val fill: Colour, val width: BordureWidth): ExtraPattern(ExtraPatternOption.Bordure) {
	companion object {
		enum class BordureWidth(override val valString: String, override val dispString: String):
				SelectOption<BordureWidth> {
			Thin("thin", "Thin"),
			Thick("thick", "Thick");
			companion object {
				private val PARSER = OptionParser(arrayMappedByValString(BordureWidth.values()))
				fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
			}
			override fun getLookupMap() = PARSER.lookupMap
		}
	}
	override fun genSVG(size: WidthHeight) = createPatternSVGBBox("bordure", size).also { view ->
		Orle.genOrleDims(0, size.h / if (width == BordureWidth.Thin) 20 else 10, size).forEachIndexed { i, dims ->
			view.appendChild(createSVGRect("bordure-$i", SVGRectAttrs(fill, dims[2], dims[3], dims[0], dims[1])))
		}
	}
	override fun writeDataToChildrenOf(patternOpts: HTMLFieldSetElement) {
		getPropChildOf(patternOpts, "fill").getFirstAsInput().setValue(fill)
		UIElements.bordureWidthSelect.value = width
	}
}

data class Canton(val currentDesign: DesignOption, val isHoistReversed: Boolean, val cantonWidthString: String,
	val content: CantonContent?,
): ExtraPattern(ExtraPatternOption.Canton) {
	companion object {
		data class CantonChargeContent(val charge: Charge?): CantonContent {
			override fun useCantonOpts(cantonBBox: SVGElement): AspectRatio {
				if (isHoistReversed) cantonBBox.style.setProperty("transform", "scaleX(-1)")
				if (charge != null) cantonBBox.appendChild(createSVGBBox(
						"canton-charge",
						SVGBBoxAttrs("-50 -50 100 100", 90, 90, 5, 5)
					)).appendChild(charge.genSVG("canton-charge"))
				return AspectRatio.Square
			}
		}
		sealed interface CantonContent {
			fun useCantonOpts(cantonBBox: SVGElement): AspectRatio?
		}
		enum class CantonContentType(override val valString: String, override val dispString: String):
				SelectOption<CantonContentType> {
			Charge("charge", "Charge"),
			Flag("flag", "Flag");
			companion object {
				private val PARSER = OptionParser(arrayMappedByValString(CantonContentType.values()))
				fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
			}
			override fun getLookupMap() = PARSER.lookupMap
		}
		data class CantonFlagContent(val presetID: String): CantonContent {
			override fun useCantonOpts(cantonBBox: SVGElement): AspectRatio {
				if (isHoistReversed) cantonBBox.style.setProperty("transform", "scaleX(-1)")
				val presetData = FlagPreset.lookupMap.getValue(presetID).data
				val cantonWidthHeight = presetData.aspect.valString.split("-").reversed().map(x100).let { WidthHeight(it[0], it[1]) }
				cantonBBox.appendChild(presetData.design.genSVG(cantonWidthHeight))
				presetData.extraPatterns.forEach {
					cantonBBox.appendChild(it.genSVG(cantonWidthHeight))
				}
				return presetData.aspect
			}
		}
		enum class CantonWidth(override val valString: String = "auto", override val dispString: String = "auto"):
				SelectOption<CantonWidth> {
			Auto, Half(1, 2), TwoFifths(2, 5), Third(1, 3), Quarter(1, 4);
			@Suppress("unused")
			constructor(a: Int, b: Int): this("$a-$b", "$a/$b")
			companion object {
				private val PARSER = OptionParser(arrayMappedByValString(CantonWidth.values()))
				fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
			}
			override fun getLookupMap() = PARSER.lookupMap
		}
		fun genWidth(cantonWidthString: String, currentDesign: DesignOption, size: WidthHeight) =
			if (cantonWidthString == "auto") when (currentDesign) {
				VBands.VDuoband, VBands.VTriband, VBands.VTetraband ->
					UIElements.viewParent.firstElementChild!!.children[1]!!.children[when (currentDesign) {
						VBands.VDuoband -> 2
						VBands.VTriband -> 3
						VBands.VTetraband -> 4
						else -> -1
					}]!!.getAttribute("width")!!.toInt()
				else -> size.w / 2
			} else cantonWidthString.split("-").let { size.w * it[0].toInt() / it[1].toInt() }
	}
	override fun genSVG(size: WidthHeight) = createPatternSVGBBox("canton", size).also { view ->
		genWidth(cantonWidthString, currentDesign, size).let { cantonWidth ->
			(content?.useCantonOpts(view) ?: AspectRatio.OneTwo).valString.split("-").map { it.toInt() }.let { ratio ->
				SVGBBoxAttrs("0 0 ${ratio[1]}00 ${ratio[0]}00", cantonWidth,
					(cantonWidth * ratio[0] / ratio[1]), if (isHoistReversed) -cantonWidth else 0).applyTo(view)
			}
		}
	}
	override fun writeDataToChildrenOf(patternOpts: HTMLFieldSetElement) {
		(document.getElementById("canton-canton-content") as HTMLFieldSetElement?)?.let {
			removeSectionChild("canton", "canton-content")
		}
		getPropChildOf(patternOpts, "canton-width").getFirstAsSelect().value = cantonWidthString
		CantonContentType.limitSelect(getPropChildOf(patternOpts, "canton-content-type").getFirstAsSelect()).value = when (content) {
			is CantonChargeContent -> CantonContentType.Charge
			else -> CantonContentType.Flag
		}
		addSectionChild("canton", "canton-content")
		if (content == null) return
		val cantonOpts = document.gEBI("canton-canton-content") as HTMLFieldSetElement
		when (content) {
			is CantonChargeContent -> content.charge?.writeToUI(cantonOpts)
			is CantonFlagContent -> getPropChildOf(cantonOpts, "flag").getFirstAsSelect().value = content.presetID
		}
	}
}

data class Chevron(val chevronWidth: ChevronWidth, val fill: Colour, val fimbriation: ChevronFimbriation? = null):
		ExtraPattern(ExtraPatternOption.Chevron) {
	companion object {
		data class ChevronFimbriation(val fill: Colour, val width: SingleFimbriationWidth)
		enum class ChevronWidth(override val valString: String, override val dispString: String):
				SelectOption<ChevronWidth> {
			Auto("auto", "to square"),
			ThreeQuarters("9", "3/4"), TwoThirds("8", "2/3"), Half("6", "1/2"), Third("4", "1/3"), Quarter("3", "1/4");
			companion object {
				private val PARSER = OptionParser(arrayMappedByValString(ChevronWidth.values()))
				fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
			}
			override fun getLookupMap() = PARSER.lookupMap
		}
	}
	override fun genSVG(size: WidthHeight) = createPatternSVGBBox("chevron", size).also { view ->
		val pointList = PointList(MutableDoublePoint(0.0, 0.0),
			MutableDoublePoint(size.w / 2.0, size.h / 2.0), MutableDoublePoint(0.0, size.h.toDouble()))
		pointList.points[1].x *= if (chevronWidth == ChevronWidth.Auto) size.h / size.w.toDouble()
			else chevronWidth.valString.toInt() / 6.0
		fimbriation?.also {
			view.appendChild(createSVGPolygon("chevron-fimb", SVGPolygonAttrs(it.fill, pointList.toString())))
			pointList.points[0].y = size.h / 10.0
			if (it.width == SingleFimbriationWidth.Thin) {
				pointList.points[0].y /= 2.0
				pointList.points[1].x *= 0.9
			} else {
				pointList.points[1].x *= 0.8
			}
			pointList.points[2].y -= pointList.points[0].y
		}
		view.appendChild(createSVGPolygon("chevron", SVGPolygonAttrs(fill, pointList.toString())))
	}
	override fun writeDataToChildrenOf(patternOpts: HTMLFieldSetElement) {
		(document.gEBI("chevron-single-fimb") as HTMLFieldSetElement?)?.let {
			removeSectionChild("chevron", "single-fimb")
		}
		getPropChildOf(patternOpts, "fill").getFirstAsInput().setValue(fill)
		UIElements.chevronWidthSelect.value = chevronWidth
		fimbriation?.let {
			SingleFimbriationWidth.limitSelect(getPropChildOf(patternOpts, "single-fimb-type").getFirstAsSelect()).value = it.width
			val fimb = document.gEBI("chevron-single-fimb") as HTMLFieldSetElement
			getPropChildOf(fimb, "fill").getFirstAsInput().setValue(it.fill)
			//TODO canton fill
		}
	}
}

data class Frame(val fill: Colour, val frameData: FrameShape): ExtraPattern(ExtraPatternOption.Frame) {
	companion object {
		enum class FrameShape(override val valString: String, override val dispString: String,
				val pathGen: (WidthHeight) -> String): SelectOption<FrameShape> {
			Isosceles("isosceles", "Isosceles triangle", { "M0,0 h${it.w} v${it.h} L${it.w / 2},0 L0,${it.h} Z" }),
			InvIsosceles("isosceles-inv", "Isosceles triangle (inverted)",
				{ "M0,0 L${it.w / 2},${it.h} L${it.w},0 v${it.h} h-${it.w} Z" });
			companion object {
				private val PARSER = OptionParser(arrayMappedByValString(FrameShape.values()))
				fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
			}
			override fun getLookupMap() = PARSER.lookupMap
		}
	}
	override fun genSVG(size: WidthHeight) = createPatternSVGBBox("frame", size).also { view ->
		view.appendChild(createSVGPath("frame-${frameData.valString}", SVGPathAttrs(fill, frameData.pathGen(size))))
	}
	override fun writeDataToChildrenOf(patternOpts: HTMLFieldSetElement) {
		getPropChildOf(patternOpts, "fill").getFirstAsInput().setValue(fill)
		UIElements.frameShapeSelect.value = frameData
	}
}

data class HoistBand(val fill: Colour, val width: HoistBandWidth): ExtraPattern(ExtraPatternOption.HoistBand) {
	companion object {
		enum class HoistBandWidth(val a: Int): SelectOption<HoistBandWidth> {
			Third(3), Quarter(4), Fifth(5);
			override val dispString = "1/$a"
			override val valString = "$a"
			companion object {
				private val PARSER = OptionParser(arrayMappedByValString(HoistBandWidth.values()))
				fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
			}
			override fun getLookupMap() = PARSER.lookupMap
		}
	}
	override fun genSVG(size: WidthHeight) = createPatternSVGBBox("hoist-band", size).also { view ->
		view.appendChild(createSVGRect("hoist-band", SVGRectAttrs(fill, size.w / width.a, size.h)))
	}
	override fun writeDataToChildrenOf(patternOpts: HTMLFieldSetElement) {
		getPropChildOf(patternOpts, "fill").getFirstAsInput().setValue(fill)
		UIElements.hoistBandWidthSelect.value = width
	}
}

data class Orle(val width: OrleWidth, val fill: Colour): ExtraPattern(ExtraPatternOption.Orle) {
	companion object {
		enum class OrleWidth(override val valString: String, override val dispString: String): SelectOption<OrleWidth> {
			Thin("thin", "Thin"),
			Thick("thick", "Thick");
			companion object {
				private val PARSER = OptionParser(arrayMappedByValString(OrleWidth.values()))
				fun limitSelect(element: HTMLSelectElement) = EnumLimitedSelect(element, PARSER)
			}
			override fun getLookupMap() = PARSER.lookupMap
		}
		fun genOrleDims(margin: Int, width: Int, size: WidthHeight) = arrayOf(
			arrayOf(margin, margin, size.w - 2 * margin, width),
			arrayOf(margin, margin, width, size.h - 2 * margin),
			arrayOf(margin, size.h - margin - width, size.w - 2 * margin, width),
			arrayOf(size.w - margin - width, margin, width, size.h - 2 * margin))
	}
	override fun genSVG(size: WidthHeight) = createPatternSVGBBox("orle", size).also { view ->
		(size.h / 20).let { margin -> genOrleDims(margin, margin * if (width == OrleWidth.Thin) 1 else 2, size) }
			.forEachIndexed { i, dims ->
				view.appendChild(createSVGRect("orle-$i", SVGRectAttrs(fill, dims[2], dims[3], dims[0], dims[1])))
			}
	}
	override fun writeDataToChildrenOf(patternOpts: HTMLFieldSetElement) {
		getPropChildOf(patternOpts, "fill").getFirstAsInput().setValue(fill)
		UIElements.orleWidthSelect.value = width
	}
}

data class CustomFrames(val frames: Map<Int, Pair<Colour, String>>): ExtraPattern(ExtraPatternOption.CustomFrame) {
	override fun genSVG(size: WidthHeight) = createPatternSVGBBox("custom-frame", size).also { view ->
		frames.forEach { (i, frameData) ->
			view.appendChild(createSVGPath("custom-frame-$i", SVGPathAttrs(frameData.first, frameData.second)))
		}
	}
	override fun writeDataToChildrenOf(patternOpts: HTMLFieldSetElement) = frames.forEach { (i, frameData) ->
		addCustomFrameAndUpdate()
		(document.gEBI("custom-frame-$i") as HTMLFieldSetElement).let { frameOpts ->
			getPropChildOf(frameOpts, "fill").getFirstAsInput().setValue(frameData.first)
			getPropChildOf(frameOpts, "path").getFirstAsInput().setValue(frameData.second)
		}
	}
}
