@file:Suppress("SpellCheckingInspection", "UNUSED_VARIABLE")

import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

plugins {
	kotlin("multiplatform") version "1.6.10"
	id("com.github.ben-manes.versions") version "0.42.0"
}

repositories {
	mavenCentral()
	maven("https://maven.pkg.jetbrains.space/public/p/kotlinx-html/maven")
}

kotlin {
	js(IR) {
		browser {
			binaries.executable() // :jsBrowserDevelopmentRun
		}
	}

//	explicitApi()

	sourceSets {
		fun creatingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {}) = creating {
			parent?.let(::dependsOn)
			configuration(this)
		}

		fun gettingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {}) = getting {
			parent?.let(::dependsOn)
			configuration(this)
		}

		fun depStr(partialCoords: String, version: String) = "$partialCoords:$version"

		fun kotlinx(module: String, version: String) = depStr("org.jetbrains.kotlinx:kotlinx-$module", version)

		val commonMain by gettingWithParent(null)

		val jsMain by gettingWithParent(commonMain) {
			resources.srcDir("src/resources/js")
			dependencies {
				implementation(kotlinx("html", "0.7.3"))
			}
		}

		all {
			kotlin.srcDir("src/${name.takeLast(4).toLowerCase()}/${name.dropLast(4)}")
			listOf(
				"kotlin.js.ExperimentalJsExport"
			).forEach(languageSettings::optIn)
		}
	}
}

tasks.withType<com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask> {
	rejectVersionIf {
		candidate.version.toLowerCase().let {
			it.contains("-rc") || it.contains("-m") || it.contains("-beta") || it.contains("-alpha")
		}
	}
}
