# VexilLogos — A one-screen web app for vexillography
![build targets: Kotlin/JS](https://img.shields.io/badge/build_targets-Kotlin/JS-7F5FBF.svg?logo=kotlin&longCache=true&style=flat-square)
![node dependencies: 0](https://img.shields.io/badge/node_dependencies-0-brightgreen.svg?logo=npm&longCache=true&style=flat-square)

## Usage
The master branch of this repo is hosted on GitLab Pages, so you can [use it right in the browser here](https://yoshirulz.gitlab.io/VexilLogos).

Every option in the side pane is clearly labelled, though you'll find some buttons and dropdowns have no effect — this is very much a work in progress. There's also a randomizer at the bottom of the pane if you need a little inspiration (be warned: it has no taste).

## License
VexilLogos flag design tool — Licensed under GPL2+ (GPL-2.0)

Copyright © 2018 YoshiRulz <[OSSYoshiRulz@gmail.com](mailto:OSSYoshiRulz@gmail.com)>  
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.  
You should have received [a copy of the GNU General Public License](https://gitlab.com/YoshiRulz/VexilLogos/blob/master/LICENSE.md) along with this program. If not, see [gnu.org/licenses](https://www.gnu.org/licenses).
